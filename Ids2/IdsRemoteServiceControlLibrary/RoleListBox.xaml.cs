﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using IdsControlLibrary.Utils;
using Protocol.Data;

namespace IdsRemoteServiceControlLibrary
{
    /// <summary>
    ///     Interaction logic for RoleListBox.xaml
    /// </summary>
    public partial class RoleListBox : UserControl
    {
        private List<RoleEntry> _Roles;
        private readonly object LockObject = new object();

        public delegate void SelectionChangedEvent( int index, Role entry );

        public event SelectionChangedEvent Checked, UnChecked;

        public class RoleEntry : Role
        {
            public bool Checked { get; set; }
        }

        public RoleListBox()
        {
            if( !DesignerProperties.GetIsInDesignMode( this ) )
            {
                Task.Run( () =>
                {
                    var Srv = Globals.Service;

                    var Rls = ( from R in Srv.GetRoles()
                                select new RoleEntry
                                       {
                                           Name = R.Name,
                                           Manditory = R.Manditory
                                       } ).ToList();
                    lock( LockObject )
                        _Roles = Rls;
                    Roles = Rls;
                } );
            }
            InitializeComponent();
        }

        public List<RoleEntry> Roles
        {
            get
            {
                while( true )
                {
                    List<RoleEntry> R;
                    lock( LockObject )
                        R = _Roles;
                    if( R != null )
                        break;
                    Thread.Sleep( 100 );
                }
                return (List<RoleEntry>)GetValue( RolesProperty );
            }
            set => Dispatcher.Invoke( () =>
            {
                SetValue( RolesProperty, value );
                RolesListBox.UpdateLayout();
            } );
        }

        // Using a DependencyProperty as the backing store for Roles.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RolesProperty =
            DependencyProperty.Register( "Roles", typeof( List<RoleEntry> ), typeof( RoleListBox ) );

        private static void SelectEntry( object sender )
        {
            if( sender is CheckBox Sender )
            {
                var Item = Sender.FindParent<ListBoxItem>();
                if( Item != null )
                    Item.IsSelected = true;
            }
        }

        private void CheckBox_Unchecked( object sender, RoutedEventArgs e )
        {
            if( UnChecked != null )
            {
                SelectEntry( sender );

                var Ndx = RolesListBox.SelectedIndex;

                if( Ndx >= 0 )
                    UnChecked?.Invoke( Ndx, _Roles[ Ndx ] );
            }
        }

        private void DockPanel_MouseDown( object sender, MouseButtonEventArgs e )
        {
            if( sender is DockPanel Sender )
            {
                var CheckBox = Sender.FindChild<CheckBox>();
                CheckBox.IsChecked = !CheckBox.IsChecked;
            }
        }

        private void CheckBox_Checked( object sender, RoutedEventArgs e )
        {
            if( Checked != null )
            {
                SelectEntry( sender );

                var Ndx = RolesListBox.SelectedIndex;

                if( Ndx >= 0 )
                    Checked?.Invoke( Ndx, _Roles[ Ndx ] );
            }
        }
    }
}