﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using IdsControlLibrary.Utils;
using Protocol.Data;

namespace IdsRemoteServiceControlLibrary
{
    /// <summary>
    ///     Interaction logic for RoleListBox.xaml
    /// </summary>
    public partial class ZoneListBox
    {
        private List<ZoneEntry> _Zones;
        private readonly object LockObject = new object();

        public delegate void SelectionChangedEvent( int index, Zone entry );

        public event SelectionChangedEvent Checked, UnChecked;

        public class ZoneEntry : Zone
        {
            public bool Checked { get; set; }
        }

        public ZoneListBox()
        {
            if( !DesignerProperties.GetIsInDesignMode( this ) )
            {
                Task.Run( () =>
                {
                    var Srv = Globals.Service;

                    var Rls = ( from Z in Srv.GetZones()
                                orderby Z.Name
                                select new ZoneEntry
                                       {
                                           Name = Z.Name,
                                           Abbreviation = Z.Abbreviation,
                                           SortIndex = Z.SortIndex
                                       } ).ToList();
                    lock( LockObject )
                        _Zones = Rls;
                    Zones = Rls;
                } );
            }
            InitializeComponent();
        }

        public List<ZoneEntry> Zones
        {
            get
            {
                while( true )
                {
                    List<ZoneEntry> R;
                    lock( LockObject )
                        R = _Zones;
                    if( R != null )
                        break;
                    Thread.Sleep( 100 );
                }
                return (List<ZoneEntry>)GetValue( ZonesProperty );
            }
            set => Dispatcher.Invoke( () =>
            {
                SetValue( ZonesProperty, value );
                ZonesListBox.UpdateLayout();
            } );
        }

        // Using a DependencyProperty as the backing store for Roles.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ZonesProperty =
            DependencyProperty.Register( "Zones", typeof( List<ZoneEntry> ), typeof( ZoneListBox ) );

        private static void SelectEntry( object sender )
        {
            if( sender is CheckBox Sender )
            {
                var Item = Sender.FindParent<ListBoxItem>();
                if( Item != null )
                    Item.IsSelected = true;
            }
        }

        private void CheckBox_Unchecked( object sender, RoutedEventArgs e )
        {
            if( UnChecked != null )
            {
                SelectEntry( sender );

                var Ndx = ZonesListBox.SelectedIndex;

                if( Ndx >= 0 )
                    UnChecked?.Invoke( Ndx, _Zones[ Ndx ] );
            }
        }

        private void DockPanel_MouseDown( object sender, MouseButtonEventArgs e )
        {
            if( sender is DockPanel Sender )
            {
                var CheckBox = Sender.FindChild<CheckBox>();
                CheckBox.IsChecked = !CheckBox.IsChecked;
            }
        }

        private void CheckBox_Checked( object sender, RoutedEventArgs e )
        {
            if( Checked != null )
            {
                SelectEntry( sender );

                var Ndx = ZonesListBox.SelectedIndex;

                if( Ndx >= 0 )
                    Checked?.Invoke( Ndx, _Zones[ Ndx ] );
            }
        }
    }
}