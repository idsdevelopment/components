﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace IdsRemoteServiceControlLibrary
{
    /// <summary>
    ///     Interaction logic for PasswordInput.xaml
    /// </summary>
    public partial class PasswordInput : UserControl
    {
        public bool ValidPassword
        {
            get => (bool)GetValue( ValidPasswordProperty );
            private set => SetValue( ValidPasswordProperty, value );
        }

        // Using a DependencyProperty as the backing store for ValidPassword.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ValidPasswordProperty =
            DependencyProperty.Register( "ValidPassword", typeof( bool ), typeof( PasswordInput ), new PropertyMetadata( false ) );

        private readonly object LockObject = new object();

        private int MinLength, MinUpper, MinLower, MinNumeric, MinSymbols;

        private void Image_MouseDown( object sender, MouseButtonEventArgs e )
        {
            e.Handled = true;
            ShowPassword.Text = PasswordInputBox.Password;
            ShowConfirm.Text = Confirm.Password;
            PasswordInputBox.Visibility = Visibility.Collapsed;
            Confirm.Visibility = Visibility.Collapsed;
            ShowPassword.UpdateLayout();
        }

        private void Image_MouseUp( object sender, MouseButtonEventArgs e )
        {
            e.Handled = true;
            PasswordInputBox.Visibility = Visibility.Visible;
            Confirm.Visibility = Visibility.Visible;
        }

        private bool EnablePassword()
        {
            Visibility Ok, Error;
            var Valid = IsPasswordValid;
            if( Valid )
            {
                Ok = Visibility.Visible;
                Error = Visibility.Collapsed;
            }
            else
            {
                Ok = Visibility.Collapsed;
                Error = Visibility.Visible;
            }

            PasswordOk.Visibility = Ok;
            PasswordError.Visibility = Error;
            return Valid;
        }

        private bool EnableConfirm()
        {
            Visibility Ok, Error;
            var Valid = IsConfirmValid;
            if( Valid )
            {
                Ok = Visibility.Visible;
                Error = Visibility.Collapsed;
            }
            else
            {
                Ok = Visibility.Collapsed;
                Error = Visibility.Visible;
            }

            ConfirmOk.Visibility = Ok;
            ConfirmError.Visibility = Error;
            return Valid;
        }

        private void DoEnable()
        {
            ValidPassword = EnablePassword() & EnableConfirm();
        }

        private void Password_KeyUp( object sender, KeyEventArgs e )
        {
            DoEnable();
        }

        private void Confirm_KeyUp( object sender, KeyEventArgs e )
        {
            DoEnable();
        }

        private void ImageButton_MouseLeave( object sender, MouseEventArgs e )
        {
            if( sender is Button Sender )
                Sender.Background = Brushes.Transparent;
        }

        private void ImageButton_MouseEnter( object sender, MouseEventArgs e )
        {
            if( sender is Button Sender )
                Sender.Background = Brushes.DarkGray;
        }

        public PasswordInput()
        {
            if( !DesignerProperties.GetIsInDesignMode( this ) )
            {
                Task.Run( () =>
                {
                    var Srv = Globals.Service;

                    var P = Srv.GetUserPreferences();
                    lock( LockObject )
                    {
                        foreach( var Preference in P )
                        {
                            switch( Preference.DevicePreference.Trim().ToUpper() )
                            {
                            case "PASSWORD_MIN_UPPER":
                                MinUpper = Preference.IntValue;
                                break;

                            case "PASSWORD_MIN_LOWER":
                                MinLower = Preference.IntValue;
                                break;

                            case "PASSWORD_MIN_NUMERIC":
                                MinNumeric = Preference.IntValue;
                                break;

                            case "PASSWORD_MIN_SYMBOL":
                                MinSymbols = Preference.IntValue;
                                break;

                            case "PASSWORD_MIN_LENGTH":
                                MinLength = Preference.IntValue;
                                break;
                            }
                        }

                        MinLength = Math.Max( MinLength, MinUpper + MinLower + MinNumeric + MinSymbols );
                        if( MinLength < 1 )
                            MinLength = 1;
                    }
                } );
            }

            InitializeComponent();

            PasswordOk.Visibility = Visibility.Collapsed;
            ConfirmOk.Visibility = Visibility.Collapsed;
        }

        public bool IsConfirmValid => PasswordInputBox.Password == Confirm.Password;

        public string Password
        {
            get
            {
                var Pw = "";
                Dispatcher.Invoke( () => { Pw = PasswordInputBox.Password; } );
                return Pw;
            }
        }

        public bool IsPasswordValid
        {
            get
            {
                while( true )
                {
                    bool LenOk;

                    lock( LockObject )
                        LenOk = MinLength > 0;

                    if( !LenOk )
                        Thread.Sleep( 100 );
                    else
                        break;
                }

                var Pass = PasswordInputBox.Password;
                if( Pass.Length >= MinLength )
                {
                    var Upper = 0;
                    var Lower = 0;
                    var Numeric = 0;
                    var Symbols = 0;

                    foreach( var C in Pass )
                    {
                        if( ( C >= '0' ) && ( C <= '9' ) )
                            ++Numeric;
                        else if( ( C >= 'A' ) && ( C <= 'Z' ) )
                            ++Upper;
                        else if( ( C >= 'z' ) && ( C <= 'z' ) )
                            ++Lower;
                        else
                            ++Symbols;
                    }
                    return ( Upper >= MinUpper ) && ( Lower >= MinLower ) && ( Numeric >= MinNumeric ) && ( Symbols >= MinSymbols );
                }
                return false;
            }
        }
    }
}