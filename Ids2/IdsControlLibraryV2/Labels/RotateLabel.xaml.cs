﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using IdsControlLibraryV2.Annotations;

namespace IdsControlLibraryV2
{
    /// <summary>
    ///     Interaction logic for RotateLabel.xaml
    /// </summary>
    public partial class RotateLabel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        [ NotifyPropertyChangedInvocator ]
        protected virtual void OnPropertyChanged( [ CallerMemberName ] string propertyName = null )
        {
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        public RotateLabel()
        {
            InitializeComponent();
        }

        public string Text
        {
            get => (string)GetValue( TextProperty );
            set
            {
                SetValue( TextProperty, value );
                OnPropertyChanged( nameof( Text ) );
            }
        }

        // Using a DependencyProperty as the backing store for Text.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register( nameof( Text ), typeof( string ), typeof( RotateLabel ), new FrameworkPropertyMetadata( "", TextPropertyChangedCallback ) );

        private static void TextPropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
        {
            if( dependencyObject is RotateLabel Label )
                Label.Text = (string)dependencyPropertyChangedEventArgs.NewValue;
        }

        public double Angle
        {
            get => (double)GetValue( AngleProperty );
            set
            {
                SetValue( AngleProperty, value );
                OnPropertyChanged( nameof( Angle ) );
            }
        }

        // Using a DependencyProperty as the backing store for Angle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AngleProperty =
            DependencyProperty.Register( nameof( Angle ), typeof( double ), typeof( RotateLabel ), new FrameworkPropertyMetadata( -90.0, AnglePropertyChangedCallback ) );

        private static void AnglePropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
        {
            if( dependencyObject is RotateLabel Label )
                Label.Angle = (double)dependencyPropertyChangedEventArgs.NewValue;
        }
    }
}