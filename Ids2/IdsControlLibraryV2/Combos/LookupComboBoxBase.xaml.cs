﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using IdsControlLibraryV2.Annotations;
using IdsControlLibraryV2.Virtualisation;
using IdsControlLibraryV2.Virtualisation.LookupCombo;

namespace IdsControlLibraryV2
{
    public class ComboBoxLookupItem
    {
        public ComboBoxLookupItem Self => this;

        public int Index { get; set; }
        public bool IsEvenRow => (Index & 1) == 0;
        public virtual string SotrKey => LookupKey;
        public virtual string LookupKey => "";

        public virtual bool ShowSecondLine => !string.IsNullOrEmpty(SecondLineText);
        public virtual string SecondLineText => "";

        public virtual bool ShowThirdLine => !string.IsNullOrEmpty(ThirdLineText);
        public virtual string ThirdLineText => "";

        // Place holders. Gets Replaced
        public Brush EvenRowBackground { get; set; }
        public Brush OddRowBackground { get; set; }
        public Brush EvenRowForeground { get; set; }
        public Brush OddRowForeground { get; set; }

        public override string ToString()
        {
            return LookupKey;
        }
    }


    internal class LookupComboBoxBoolToBackgroundColourConvertor : IValueConverter
    {
        public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
        {
            if( value is ComboBoxLookupItem LookupItem )
                return LookupItem.IsEvenRow ? LookupItem.EvenRowBackground : LookupItem.OddRowBackground;
            return Brushes.White;
        }

        public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
        {
            throw new NotImplementedException();
        }
    }

    internal class LookupComboBoxBoolToForegroundColourConvertor : IValueConverter
    {
        public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
        {
            if( value is ComboBoxLookupItem LookupItem )
                return LookupItem.IsEvenRow ? LookupItem.EvenRowForeground : LookupItem.OddRowForeground;
            return Brushes.Black;
        }

        public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    ///     Interaction logic for LookupComboBoxBase.xaml
    /// </summary>
    public partial class LookupComboBoxBase : INotifyPropertyChanged
    {
        public const int DEFAULT_PREFETCH = 30;

        public Func<ComboBoxLookupItem> OnCreateItem;

        public LookupComboBoxBase()
        {
            InitializeComponent();
            AddButton.Visibility = Visibility.Collapsed;
            DataContext = Root;
        }

        [ Category( "Brush" ) ]
        public Brush OddRowBackground
        {
            get => (Brush)GetValue( OddRowBackgroundProperty );
            set
            {
                SetValue( OddRowBackgroundProperty, value );
                OnPropertyChanged( nameof( OddRowBackground ) );
            }
        }

        // Using a DependencyProperty as the backing store for OddRowBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty OddRowBackgroundProperty =
            DependencyProperty.Register( nameof( OddRowBackground ), typeof( Brush ), typeof( LookupComboBoxBase ), new FrameworkPropertyMetadata( Brushes.White, OddRowBackgroundPropertyChangedCallback ) );

        private static void OddRowBackgroundPropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
        {
            if( dependencyObject is LookupComboBoxBase Combo && dependencyPropertyChangedEventArgs.NewValue is Brush Brush )
                Combo.OddRowBackground = Brush;
        }

        [ Category( "Brush" ) ]
        public Brush OddRowForeground
        {
            get => (Brush)GetValue( OddRowForegroundProperty );
            set
            {
                SetValue( OddRowForegroundProperty, value );
                OnPropertyChanged( nameof( OddRowForeground ) );
            }
        }

        // Using a DependencyProperty as the backing store for OddRowForegroundBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty OddRowForegroundProperty =
            DependencyProperty.Register( nameof( OddRowForeground ), typeof( Brush ), typeof( LookupComboBoxBase ), new FrameworkPropertyMetadata( Brushes.Black, OddRowForegroundPropertyChangedCallback ) );

        private static void OddRowForegroundPropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
        {
            if( dependencyObject is LookupComboBoxBase Combo && dependencyPropertyChangedEventArgs.NewValue is Brush Brush )
                Combo.OddRowForeground = Brush;
        }

        [ Category( "Brush" ) ]
        public Brush EvenRowBackground
        {
            get => (Brush)GetValue( EvenRowBackgroundProperty );
            set
            {
                SetValue( EvenRowBackgroundProperty, value );
                OnPropertyChanged( nameof( EvenRowBackground ) );
            }
        }

        // Using a DependencyProperty as the backing store for EvenRowBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty EvenRowBackgroundProperty =
            DependencyProperty.Register( nameof( EvenRowBackground ), typeof( Brush ), typeof( LookupComboBoxBase ), new FrameworkPropertyMetadata( Brushes.BlanchedAlmond, EvenRowBrushPropertyChangedCallback ) );

        private static void EvenRowBrushPropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
        {
            if( dependencyObject is LookupComboBoxBase Combo && dependencyPropertyChangedEventArgs.NewValue is Brush Brush )
                Combo.EvenRowBackground = Brush;
        }

        [ Category( "Brush" ) ]
        public Brush EvenRowForeground
        {
            get => (Brush)GetValue( EvenRowForegroundProperty );
            set
            {
                SetValue( EvenRowForegroundProperty, value );
                OnPropertyChanged( nameof( EvenRowForeground ) );
            }
        }

        // Using a DependencyProperty as the backing store for EvenRowForegroundBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty EvenRowForegroundProperty =
            DependencyProperty.Register( nameof( EvenRowForeground ), typeof( Brush ), typeof( LookupComboBoxBase ), new FrameworkPropertyMetadata( Brushes.Black, EvenRowForegroundPropertyChangedCallback ) );

        private static void EvenRowForegroundPropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
        {
            if( dependencyObject is LookupComboBoxBase Combo && dependencyPropertyChangedEventArgs.NewValue is Brush Brush )
                Combo.EvenRowForeground = Brush;
        }

        [ Category( "Common" ) ]
        public ILookupComboBoxVirtualObservableCollection ItemsSource
        {
            get => (ILookupComboBoxVirtualObservableCollection)GetValue( ItemsSourceProperty );
            set
            {
                SetValue( ItemsSourceProperty, value );
                OnPropertyChanged( nameof( ItemsSource ) );
            }
        }

        // Using a DependencyProperty as the backing store for ItemsSource.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register( nameof( ItemsSource ), typeof( IVirtualObservableCollection ), typeof( LookupComboBoxBase ),
                                         new FrameworkPropertyMetadata( null, ItemsSourcePropertyChangedCallback ) );

        private static void ItemsSourcePropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
        {
            if( dependencyObject is LookupComboBoxBase Combo && dependencyPropertyChangedEventArgs.NewValue is ILookupComboBoxVirtualObservableCollection ItemsSource )
            {
                ItemsSource.EvenRowBackground = Combo.EvenRowBackground;
                ItemsSource.EvenRowForeground = Combo.EvenRowForeground;
                ItemsSource.OddRowBackground = Combo.OddRowBackground;
                ItemsSource.OddRowForeground = Combo.OddRowForeground;

                ItemsSource.OnCollectionChanged = enumerable =>
                {
                    var Ndx = 0;
                    foreach( var I in enumerable )
                    {
                        if( I is ComboBoxLookupItem Item )
                            Item.Index = Ndx++;
                    }
                };

                Combo.LookupCombo.ItemsSource = ItemsSource;
                ItemsSource.First();
            }
        }

        [ Category( "Common" ) ]
        public int SelectedIndex
        {
            get => (int)GetValue( SelectedIndexProperty );
            set
            {
                SetValue( SelectedIndexProperty, value );
                OnPropertyChanged( nameof( SelectedIndex ) );
            }
        }

        // Using a DependencyProperty as the backing store for SelectedIndex.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedIndexProperty =
            DependencyProperty.Register( nameof( SelectedIndex ), typeof( int ), typeof( LookupComboBoxBase ), new FrameworkPropertyMetadata( -1, SelectedIndexPropertyChangedCallback ) );

        private static void SelectedIndexPropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
        {
            if( dependencyObject is LookupComboBoxBase Combo && dependencyPropertyChangedEventArgs.NewValue is int Index )
                Combo.ItemsSource.SelectedIndex = Index;
        }

        [ Category( "Common" ) ]
        public string Text
        {
            get => (string)GetValue( TextProperty );
            set
            {
                SetValue( TextProperty, value );
                OnPropertyChanged( nameof( Text ) );
            }
        }

        // Using a DependencyProperty as the backing store for Text.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register( nameof( Text ), typeof( string ), typeof( LookupComboBoxBase ), new FrameworkPropertyMetadata( "", TextPropertyChangedCallback ) );

        private static void TextPropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
        {
            if( dependencyObject is LookupComboBoxBase Combo && dependencyPropertyChangedEventArgs.NewValue is string TextValue )
                Combo.Text = TextValue;
        }

        [ Category( "Options" ) ]
        public bool EnableNew
        {
            get => (bool)GetValue( EnableNewProperty );
            set
            {
                SetValue( EnableNewProperty, value );
                OnPropertyChanged( nameof( EnableNew ) );
            }
        }

        // Using a DependencyProperty as the backing store for EnableNew.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty EnableNewProperty =
            DependencyProperty.Register( nameof( EnableNew ), typeof( bool ), typeof( LookupComboBoxBase ), new FrameworkPropertyMetadata( false, EnableNewPropertyChangedCallback ) );

        private static void EnableNewPropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
        {
            if( dependencyObject is LookupComboBoxBase Combo && dependencyPropertyChangedEventArgs.NewValue is bool Enable )
                Combo.AddButton.Visibility = Enable ? Visibility.Visible : Visibility.Collapsed;
        }

        [ Category( "Options" ) ]
        public int PreFetch
        {
            get => (int)GetValue( PreFetchProperty );
            set
            {
                SetValue( PreFetchProperty, value );
                OnPropertyChanged( nameof( PreFetch ) );
            }
        }

        // Using a DependencyProperty as the backing store for PreFetch.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PreFetchProperty =
            DependencyProperty.Register( nameof( PreFetch ), typeof( int ), typeof( LookupComboBoxBase ), new FrameworkPropertyMetadata( DEFAULT_PREFETCH, PreFetchPropertyChangedCallback ) );

        private static void PreFetchPropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
        {
            if( dependencyObject is LookupComboBoxBase Combo && dependencyPropertyChangedEventArgs.NewValue is int Count )
                Combo.PreFetch = Count;
        }

        public event Action<object, SelectionChangedEventArgs> SelectionChanged;

        public event PropertyChangedEventHandler PropertyChanged;

        [ NotifyPropertyChangedInvocator ]
        protected virtual void OnPropertyChanged( [ CallerMemberName ] string propertyName = null )
        {
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        private void UserControl_IsEnabledChanged( object sender, DependencyPropertyChangedEventArgs e )
        {
            if( sender is LookupComboBoxBase Combo && e.NewValue is bool Enabled )
            {
                Combo.LookupCombo.IsEnabled = Enabled;
                Combo.AddButton.IsEnabled = Enabled;
                Combo.AddImage.Opacity = Enabled ? 1 : 0.2;
            }
        }

        private enum STATE
        {
            SELECTION,
            INPUT
        }

        private STATE State;

        private void LookupCombo_SelectionChanged( object sender, SelectionChangedEventArgs e )
        {
            if( sender is ComboBox Combo )
            {
                if( Combo.SelectedItem is ComboBoxLookupItem Item )
                {
                    State = STATE.SELECTION;
                    var Ndx = Item.Index;
                    SelectedIndex = Ndx;
                    if( Ndx > ItemsSource.Count - PreFetch )
                        ItemsSource.FindRange( Item.LookupKey, PreFetch );
                }
            }
            OnSelectionChanged( this, e );
        }

        private void LookupCombo_Loaded( object sender, RoutedEventArgs e )
        {
            if( sender is ComboBox Combo )
            {
                if( Combo.Template.FindName( "PART_EditableTextBox", Combo ) is TextBox TextBox )
                {
                    State = STATE.INPUT;

                    TextBox.TextChanged += ( o, args ) =>
                    {
                        if( State == STATE.SELECTION )
                        {
                            State = STATE.INPUT;
                            Combo.IsDropDownOpen = true;
                        }
                        else
                        {
                            var TxtBox = (TextBox)o;

                            var SearchText = TxtBox.Text;

                            if( Combo.SelectionBoxItem != null )
                            {
                                Combo.SelectedItem = null;
                                Combo.Text = SearchText;
                                TextBox.SelectionStart = SearchText.Length;
                            }

                            if( string.IsNullOrEmpty( SearchText ) )
                            {
                                Combo.Items.Filter = item => true;
                                Combo.SelectedItem = default(object);
                            }

                            Combo.Items.Filter = item =>
                                item.ToString().StartsWith( SearchText, true, CultureInfo.InvariantCulture );

                            Keyboard.ClearFocus();
                            Keyboard.Focus( TextBox );
                            TextBox.SelectionStart = TextBox.Text.Length;
                            Combo.IsDropDownOpen = true;
                        }
                    };
                }
            }

            if( LookupCombo.Template.FindName( "DropDownScrollViewer", LookupCombo ) is ScrollViewer ScrollViewer )
            {
                ScrollViewer.ScrollChanged += ( o, args ) =>
                {
                    if( LookupCombo.IsDropDownOpen )
                    {
                        var H = ScrollViewer.ScrollableHeight;
                        if( H > 0 )
                        {
                            var Offset = args.VerticalOffset;
                            if( Offset <= 2 )  // Get Prior
                                ItemsSource.SelectedIndex = 1;
                            else if (Offset >= H -3 ) // Get Next
                                ItemsSource.SelectedIndex = ItemsSource.Count - 1;
                        }
                    }
                };
            }
        }

        protected virtual void OnSelectionChanged( object arg1, SelectionChangedEventArgs arg2 )
        {
            SelectionChanged?.Invoke( arg1, arg2 );
        }

        private void LookupCombo_DropDownClosed( object sender, EventArgs e )
        {
            //Text = LookupCombo.Text;
        }
    }
}