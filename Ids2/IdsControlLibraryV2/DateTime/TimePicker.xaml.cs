﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;
using IdsControlLibraryV2.Annotations;

namespace IdsControlLibraryV2
{
    /// <summary>
    ///     Interaction logic for TimePicker.xaml
    /// </summary>
    public partial class TimePicker : INotifyPropertyChanged
    {
        private enum SPIN_STATE
        {
            HOURS,
            MINS,
            SECS,
            AM_PM
        }

        private SPIN_STATE State = SPIN_STATE.HOURS;

        public event PropertyChangedEventHandler PropertyChanged;


        [ NotifyPropertyChangedInvocator ]
        protected virtual void OnPropertyChanged( [ CallerMemberName ] string propertyName = null )
        {
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        public TimePicker()
        {
            InitializeComponent();
            LayoutRoot.DataContext = LayoutRoot;
        }

        [ Category( "Options" ) ]
        public bool ShowSeconds
        {
            get => (bool)GetValue( ShowSecondsProperty );
            set
            {
                SetValue( ShowSecondsProperty, value );
                OnPropertyChanged( nameof( ShowSeconds ) );
            }
        }

        // Using a DependencyProperty as the backing store for ShowSeconds.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowSecondsProperty =
            DependencyProperty.Register( nameof( ShowSeconds ), typeof( bool ), typeof( TimePicker ), new FrameworkPropertyMetadata( false, ShowSecondsPropertyChangedCallback ) );

        private static void ShowSecondsPropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
        {
            if( dependencyObject is TimePicker Picker )
            {
                var Show = (bool)dependencyPropertyChangedEventArgs.NewValue;
                Picker.ShowSeconds = Show;
                if( !Show )
                {
                    Picker.Seconds.IsTabStop = false;
                    Picker.Seconds.Visibility = Visibility.Collapsed;
                    Picker.SecondsDivider.Visibility = Visibility.Collapsed;
                    Picker.AmPmInput.TabIndex = 2;
                }
                else
                {
                    Picker.Seconds.IsTabStop = true;
                    Picker.Seconds.Visibility = Visibility.Visible;
                    Picker.SecondsDivider.Visibility = Visibility.Visible;
                    Picker.AmPmInput.TabIndex = 3;
                }
            }
        }

        [ Category( "Options" ) ]
        public bool AmPm
        {
            get => (bool)GetValue( AmPmProperty );
            set
            {
                SetValue( AmPmProperty, value );
                OnPropertyChanged( nameof( AmPm ) );
            }
        }

        // Using a DependencyProperty as the backing store for AmPm.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AmPmProperty =
            DependencyProperty.Register( nameof( AmPm ), typeof( bool ), typeof( TimePicker ), new FrameworkPropertyMetadata( true, AmPmPropertyChangedCallback ) );

        private static void AmPmPropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
        {
            if( dependencyObject is TimePicker Picker )
                Picker.Hours.MaximumValue = (bool)dependencyPropertyChangedEventArgs.NewValue ? 11 : 23;
        }

        public delegate void ValueChangedEvent( TimePicker sender, DateTime newValue );

        public event ValueChangedEvent ValueChanged;

        private bool _LoadingValue;
        private bool Initialised;

        private bool LoadingValue
        {
            get => _LoadingValue || !Initialised;
            set => _LoadingValue = value;
        }

        [ Category( "Options" ) ]
        public DateTime Value
        {
            get
            {
                var RetVal = new DateTime( ( (DateTime)GetValue( ValueProperty ) ).TimeOfDay.Ticks );
                if( !ShowSeconds )
                    RetVal = RetVal.AddSeconds( -RetVal.Second );
                return RetVal;
            }

            set
            {
                if( !LoadingValue )
                {
                    LoadingValue = true;
                    try
                    {
                        SetValue( ValueProperty, value );
                        OnPropertyChanged( nameof( Value ) );

                        ValueChanged?.Invoke( this, Value );
                    }
                    finally
                    {
                        LoadingValue = false;
                    }
                }
            }
        }

        // Using a DependencyProperty as the backing store for Value.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register( nameof( Value ), typeof( DateTime ), typeof( TimePicker ), new FrameworkPropertyMetadata( new DateTime(), ValuePropertyChangedCallback ) );

        private static void ValuePropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
        {
            if( dependencyObject is TimePicker Picker )
            {
                var Value = (DateTime)dependencyPropertyChangedEventArgs.NewValue;

                var H = Value.Hour;
                if( Picker.AmPm )
                {
                    if( H >= 12 )
                    {
                        H = H - 12;
                        Picker.AmPmInput.Text = "PM";
                    }
                    else
                        Picker.AmPmInput.Text = "AM";
                }
                Picker.Hours.Value = H;
                Picker.Minutes.Value = Value.Minute;
                Picker.Seconds.Value = Value.Second;
                Picker.Value = Value;
            }
        }

        private void Hours_GotFocus( object sender, RoutedEventArgs e )
        {
            State = SPIN_STATE.HOURS;
        }

        private void Minutes_GotFocus( object sender, RoutedEventArgs e )
        {
            State = SPIN_STATE.MINS;
        }

        private void Seconds_GotFocus( object sender, RoutedEventArgs e )
        {
            State = SPIN_STATE.SECS;
        }

        private void AmPmInput_GotFocus( object sender, RoutedEventArgs e )
        {
            AmPmInput.SelectAll();
            State = SPIN_STATE.AM_PM;
        }

        private void Spinner_SpinUpClick( object sender, EventArgs e )
        {
            switch( State )
            {
            case SPIN_STATE.HOURS:
                Value = Value.AddHours( 1 );
                break;
            case SPIN_STATE.MINS:
                Value = Value.AddMinutes( 1 );
                break;
            case SPIN_STATE.SECS:
                if( ShowSeconds )
                    Value = Value.AddSeconds( 1 );
                break;
            case SPIN_STATE.AM_PM:
                if( AmPm )
                    Value = Value.AddHours( 12 );
                break;
            }
        }

        private void Spinner_SpinDownClick( object sender, EventArgs e )
        {
            var V = Value.AddDays( 1 ); // Stop -ve underflow
            switch( State )
            {
            case SPIN_STATE.HOURS:
                Value = V.AddHours( -1 );
                break;
            case SPIN_STATE.MINS:
                Value = V.AddMinutes( -1 );
                break;
            case SPIN_STATE.SECS:
                if( ShowSeconds )
                    Value = V.AddSeconds( -1 );
                break;
            case SPIN_STATE.AM_PM:
                if( AmPm )
                    Value = V.AddHours( -12 );
                break;
            }
        }

        private void AmPmInput_PreviewKeyDown( object sender, KeyEventArgs e )
        {
            if( e.Key != Key.Tab )
            {
                e.Handled = true;
                Value = Value.AddHours( 12 );
            }
        }

        private void AmPmInput_PreviewKeyUp( object sender, KeyEventArgs e )
        {
            e.Handled = true;
        }

        private void UserControl_Loaded( object sender, RoutedEventArgs e )
        {
            ShowSeconds = ShowSeconds;
            Initialised = true;
        }

        private void AmPmInput_MouseWheel( object sender, MouseWheelEventArgs e )
        {
            e.Handled = true;
            Value = Value.AddHours( 12 );
        }

        private void Hours_ValueChanged( object sender, EventArgs e )
        {
            if( !LoadingValue )
            {
                var V = Value;
                Value = V.AddDays( 1 ).AddHours( -V.Hour ).AddHours( (int)Hours.Value );
            }
        }

        private void Minutes_ValueChanged( object sender, EventArgs e )
        {
            if( !LoadingValue )
            {
                var V = Value;
                Value = V.AddDays( 1 ).AddMinutes( -V.Minute ).AddMinutes( (int)Minutes.Value );
            }
        }

        private void Seconds_ValueChanged( object sender, EventArgs e )
        {
            if( !LoadingValue )
            {
                var V = Value;
                Value = V.AddDays( 1 ).AddSeconds( -V.Second ).AddHours( (int)Seconds.Value );
            }
        }
    }
}