﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace IdsControlLibraryV2.Convertors
{
    public class BoolToVisibilityConvertor : IValueConverter
    {
        public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
        {
            if( value is bool Vis )
                return Vis ? Visibility.Visible : Visibility.Collapsed;
            return Visibility.Collapsed;
        }

        public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
        {
            throw new NotImplementedException();
        }
    }

    public class TimeSpanToDateTimeConvertor : IValueConverter
    {
        public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
        {
            long Ticks;

            if( value is TimeSpan T )
                Ticks = T.Ticks;
            else
                Ticks = 0;

            return new DateTime( Ticks );
        }

        public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
        {
            long Ticks;

            if( value is DateTime D )
                Ticks = D.Ticks;
            else
                Ticks = 0;

            return new TimeSpan( Ticks );
        }
    }
}