﻿#pragma checksum "..\..\LookupComboBoxBase.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "1F388BA2D059728221A1174A4643883B2B28CA3C"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using IdsControlLibraryV2;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace IdsControlLibraryV2 {
    
    
    /// <summary>
    /// LookupComboBoxBase
    /// </summary>
    public partial class LookupComboBoxBase : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 15 "..\..\LookupComboBoxBase.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DockPanel Root;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\LookupComboBoxBase.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button AddButton;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\LookupComboBoxBase.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image AddImage;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\LookupComboBoxBase.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox LookupCombo;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/IdsControlLibraryV2;component/lookupcomboboxbase.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\LookupComboBoxBase.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 8 "..\..\LookupComboBoxBase.xaml"
            ((IdsControlLibraryV2.LookupComboBoxBase)(target)).IsEnabledChanged += new System.Windows.DependencyPropertyChangedEventHandler(this.UserControl_IsEnabledChanged);
            
            #line default
            #line hidden
            
            #line 9 "..\..\LookupComboBoxBase.xaml"
            ((IdsControlLibraryV2.LookupComboBoxBase)(target)).Initialized += new System.EventHandler(this.UserControl_Initialized);
            
            #line default
            #line hidden
            return;
            case 2:
            this.Root = ((System.Windows.Controls.DockPanel)(target));
            return;
            case 3:
            this.AddButton = ((System.Windows.Controls.Button)(target));
            return;
            case 4:
            this.AddImage = ((System.Windows.Controls.Image)(target));
            return;
            case 5:
            this.LookupCombo = ((System.Windows.Controls.ComboBox)(target));
            
            #line 21 "..\..\LookupComboBoxBase.xaml"
            this.LookupCombo.GotFocus += new System.Windows.RoutedEventHandler(this.LookupCombo_GotFocus);
            
            #line default
            #line hidden
            
            #line 21 "..\..\LookupComboBoxBase.xaml"
            this.LookupCombo.KeyDown += new System.Windows.Input.KeyEventHandler(this.LookupCombo_KeyDown);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

