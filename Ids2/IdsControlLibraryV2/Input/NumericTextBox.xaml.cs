﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using IdsControlLibraryV2.Annotations;

namespace IdsControlLibraryV2
{
    /// <summary>
    ///     Interaction logic for NumericTextBox.xaml
    /// </summary>
    public partial class NumericTextBox : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        [ NotifyPropertyChangedInvocator ]
        protected virtual void OnPropertyChanged( [ CallerMemberName ] string propertyName = null )
        {
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        private bool AllowNegative;

        public NumericTextBox()
        {
            InitializeComponent();
            LayoutRoot.DataContext = this;
        }

        private bool IsTextAllowed( string text, out decimal value )
        {
            if( AllowNegative && ( text.Length == 1 ) && ( text[ 0 ] == '-' ) )
            {
                value = 0;
                return true;
            }
            var Ok = false;
            var DecValue = MinimumValue;

            var Dec = Decimals;
            if( Dec > 0 )
            {
                if( decimal.TryParse( text, out DecValue ) )
                {
                    if( AllowNegative || ( DecValue > 0 ) )
                    {
                        var Pos = text.IndexOf( '.' );
                        if( Pos >= 0 )
                        {
                            var Txt = text.Substring( Pos + 1 );
                            Ok = Txt.Length <= Dec;
                        }
                        else
                            Ok = true;
                    }
                }
            }
            else
            {
                if( int.TryParse( text, out var IntValue ) )
                {
                    DecValue = IntValue;
                    Ok = AllowNegative || ( IntValue >= 0 );
                }
            }

            value = DecValue;

            return Ok && ( DecValue >= MinimumValue ) && ( DecValue <= MaximumValue );
        }

        private void DoSpinner( decimal val )
        {
            if( SpinnerVisible )
            {
                Dispatcher.InvokeAsync( () =>
                {
                    Spinner.DisableDown = val <= MinimumValue;
                    Spinner.DisableUp = val >= MaximumValue;
                } );
            }
        }

        private bool WasKey;

        private void TextBox_PreviewTextInput( object sender, TextCompositionEventArgs e )
        {
            var Key = e.Text[ 0 ];
            if( ( ( Key >= '0' ) && ( Key <= '9' ) ) || ( Key == '.' ) || ( Key == '-' ) || ( Key == '+' ) )
            {
                if( sender is TextBox TxtBox )
                {
                    var Ndx = TxtBox.CaretIndex;
                    var Text = TxtBox.Text;
                    var C = e.Text;

                    Text = Text.Insert( Ndx, C );
                    var Ok = IsTextAllowed( Text, out var TextValue );
                    e.Handled = !Ok;
                    if( Ok )
                    {
                        WasKey = true;
                        SetValue( TextValue );
                    }

                    return;
                }
            }
            e.Handled = true;
        }

        private string Mask;

        private void BuildMask()
        {
            var M = new string( '0', MininumDigits );
            var D = Decimals;
            if( D > 0 )
                M += $".{new string( '0', D )}";
            Mask = M;
        }

        public event EventHandler ValueChanged;

        [ Category( "Options" ) ]
        public int MininumDigits
        {
            get => (int)GetValue( MininumDigitsProperty );
            set
            {
                if( value < 1 )
                    value = 1;

                if( value > 10 )
                    value = 10;

                SetValue( MininumDigitsProperty, value );
                BuildMask();
            }
        }

        // Using a DependencyProperty as the backing store for MininumDigits.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MininumDigitsProperty =
            DependencyProperty.Register( "MininumDigits", typeof( int ), typeof( NumericTextBox ), new FrameworkPropertyMetadata( 1, FrameworkPropertyMetadataOptions.None, MinimumDigitsPropertyChangedCallback ) );

        private static void MinimumDigitsPropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
        {
            if( dependencyObject is NumericTextBox Box )
                Box.MininumDigits = (int)dependencyPropertyChangedEventArgs.NewValue;
        }

        [ Category( "Options" ) ]
        public int Decimals
        {
            get => (int)GetValue( DecimalsProperty );
            set
            {
                SetValue( DecimalsProperty, value );
                BuildMask();
            }
        }

        // Using a DependencyProperty as the backing store for Decimals.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DecimalsProperty =
            DependencyProperty.Register( "Decimals", typeof( int ), typeof( NumericTextBox ), new FrameworkPropertyMetadata( 0, FrameworkPropertyMetadataOptions.None, DecimalsPropertyChangedCallback ) );

        private static void DecimalsPropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
        {
            if( dependencyObject is NumericTextBox Box )
                Box.Decimals = (int)dependencyPropertyChangedEventArgs.NewValue;
        }

        private bool LoadingValue;

        private void SetValue( decimal value )
        {
            if( !LoadingValue )
            {
                LoadingValue = true;
                try
                {
                    value = Math.Max( Math.Min( Math.Round( value, Decimals, MidpointRounding.AwayFromZero ), MaximumValue ), MinimumValue );
                    SetValue( ValueProperty, value );
                    DoSpinner( value );
                    OnPropertyChanged( nameof( Value ) );
                    ValueChanged?.Invoke( this, EventArgs.Empty );
                }
                finally
                {
                    LoadingValue = false;
                }
            }
        }

        [ Category( "Options" ) ]
        public decimal Value
        {
            get
            {
                if( !decimal.TryParse( Input.Text.Trim(), out var Vlue ) )
                    Vlue = 0;

                Value = Vlue;
                return Vlue;
            }

            set
            {
                value = Math.Max( Math.Min( Math.Round( value, Decimals, MidpointRounding.AwayFromZero ), MaximumValue ), MinimumValue );
                if( !WasKey )
                {
                    SetValue( value );
                    Input.Text = value.ToString( Mask, CultureInfo.InvariantCulture );
                }
                WasKey = false;
            }
        }

        // Using a DependencyProperty as the backing store for Value.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register( "Value", typeof( decimal ), typeof( NumericTextBox ),
                                         new FrameworkPropertyMetadata( (decimal)0,
                                                                        FrameworkPropertyMetadataOptions.BindsTwoWayByDefault | FrameworkPropertyMetadataOptions.AffectsRender, OnValuePropertyChanged ) );

        private static void OnValuePropertyChanged( DependencyObject obj, DependencyPropertyChangedEventArgs e )
        {
            if( obj is NumericTextBox Box )
                Box.Value = (decimal)e.NewValue;
        }

        [ Category( "Options" ) ]
        public decimal MinimumValue
        {
            get => (decimal)GetValue( MinimumValueProperty );
            set
            {
                var MaxValue = MaximumValue;

                if( value > MaxValue )
                    Value = MaxValue;

                SetValue( MinimumValueProperty, value );
            }
        }

        // Using a DependencyProperty as the backing store for MinimumValue.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MinimumValueProperty =
            DependencyProperty.Register( "MinimumValue", typeof( decimal ), typeof( NumericTextBox ), new PropertyMetadata( decimal.MinValue ) );

        [ Category( "Options" ) ]
        public decimal MaximumValue
        {
            get => (decimal)GetValue( MaximumValueProperty );
            set
            {
                var Min = MaximumValue;
                if( Value < Min )
                    Value = Min;

                AllowNegative = Value < 0;

                SetValue( MaximumValueProperty, value );
            }
        }

        // Using a DependencyProperty as the backing store for MaximumValue.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MaximumValueProperty =
            DependencyProperty.Register( "MaximumValue", typeof( decimal ), typeof( NumericTextBox ), new PropertyMetadata( decimal.MaxValue ) );

        [ Category( "Options" ) ]
        public bool SpinnerVisible
        {
            get => (bool)GetValue( SpinnerVisibleProperty );
            set
            {
                SetValue( SpinnerVisibleProperty, value );
                Spinner.Visibility = value ? Visibility.Visible : Visibility.Collapsed;
                OnPropertyChanged();
            }
        }

        // Using a DependencyProperty as the backing store for SpinnerVisible.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SpinnerVisibleProperty =
            DependencyProperty.Register( "SpinnerVisible", typeof( bool ), typeof( NumericTextBox ),
                                         new FrameworkPropertyMetadata( false, SpinnerVisiblePropertyChanged ) );

        private static void SpinnerVisiblePropertyChanged( DependencyObject source, DependencyPropertyChangedEventArgs e )
        {
            if( source is NumericTextBox Box )
                Box.SpinnerVisible = (bool)e.NewValue;
        }

        [ Category( "Options" ) ]
        public bool IsCurrency
        {
            get => (bool)GetValue( IsCurrencyProperty );
            set
            {
                SetValue( IsCurrencyProperty, value );
                Currency.Visibility = value ? Visibility.Visible : Visibility.Collapsed;
                OnPropertyChanged();
            }
        }

        // Using a DependencyProperty as the backing store for IsCurrency.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsCurrencyProperty =
            DependencyProperty.Register( "IsCurrency", typeof( bool ), typeof( NumericTextBox ), new FrameworkPropertyMetadata( false, IsCurrencyPropertyChanged ) );

        private static void IsCurrencyPropertyChanged( DependencyObject source, DependencyPropertyChangedEventArgs e )
        {
            if( source is NumericTextBox Box )
                Box.IsCurrency = (bool)e.NewValue;
        }

        [ Category( "Options" ) ]
        public decimal Increment
        {
            get => (decimal)GetValue( IncrementProperty );
            set => SetValue( IncrementProperty, Math.Abs( value ) );
        }

        // Using a DependencyProperty as the backing store for Increment.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IncrementProperty =
            DependencyProperty.Register( "Increment", typeof( decimal ), typeof( NumericTextBox ), new PropertyMetadata( (decimal)1 ) );

        private void Input_GotFocus( object sender, RoutedEventArgs e )
        {
            Dispatcher.InvokeAsync( () =>
            {
                Input.SelectionStart = 0;
                Input.SelectionLength = Input.Text.Length;
            } );
        }

        private void Spinner_SpinUpClick( object sender, EventArgs e )
        {
            Value = Value + Increment;
        }

        private void Spinner_SpinDownClick( object sender, EventArgs e )
        {
            Value = Value - Increment;
        }

        private void UserControl_GotFocus( object sender, RoutedEventArgs e )
        {
            Input.Focus();
        }

        private void UserControl_MouseWheel( object sender, MouseWheelEventArgs e )
        {
            if( e.Delta > 0 )
                Spinner_SpinUpClick( sender, e );
            else
                Spinner_SpinDownClick( sender, e );
        }

        private void UserControl_Loaded( object sender, RoutedEventArgs e )
        {
            SpinnerVisible = SpinnerVisible;
            IsCurrency = IsCurrency;
            DoSpinner( Value );
        }
    }
}