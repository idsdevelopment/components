﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using IdsControlLibraryV2.Annotations;

namespace IdsControlLibraryV2
{
    /// <summary>
    ///     Interaction logic for Spinner.xaml
    /// </summary>
    public partial class Spinner : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        [ NotifyPropertyChangedInvocator ]
        protected virtual void OnPropertyChanged( [ CallerMemberName ] string propertyName = null )
        {
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        public Spinner()
        {
            InitializeComponent();
        }

        public event EventHandler SpinUpClick, SpinDownClick;

        private void SpinUp_Click( object sender, RoutedEventArgs e )
        {
            e.Handled = true;
            SpinUpClick?.Invoke( this, e );
        }

        private void SpinDown_Click( object sender, RoutedEventArgs e )
        {
            e.Handled = true;
            SpinDownClick?.Invoke( this, e );
        }

        public Brush SpinnerBackgroundBrush
        {
            get => (Brush)GetValue( SpinnerBackgroundBrushProperty );
            set
            {
                SetValue( SpinnerBackgroundBrushProperty, value );
                OnPropertyChanged( nameof( SpinnerBackgroundBrush ) );
            }
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SpinnerBackgroundBrushProperty =
            DependencyProperty.Register( nameof( SpinnerBackgroundBrush ), typeof( Brush ), typeof( Spinner ), new FrameworkPropertyMetadata( Brushes.White, FrameworkPropertyMetadataOptions.None, SpinnerBackgroundPropertyChangedCallback ) );

        private static void SpinnerBackgroundPropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
        {
            if( dependencyObject is Spinner Spinner )
                Spinner.SpinnerBackgroundBrush = (Brush)dependencyPropertyChangedEventArgs.NewValue;
        }

        public Brush SpinnerForegroundBrush
        {
            get => (Brush)GetValue( SpinnerForegroundBrushProperty );
            set
            {
                SetValue( SpinnerForegroundBrushProperty, value );
                OnPropertyChanged( nameof( SpinnerForegroundBrush ) );
            }
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SpinnerForegroundBrushProperty =
            DependencyProperty.Register( nameof( SpinnerForegroundBrush ), typeof( Brush ), typeof( Spinner ), new FrameworkPropertyMetadata( Brushes.Black, FrameworkPropertyMetadataOptions.None, SpinnerForegroundPropertyChangedCallback ) );

        private static void SpinnerForegroundPropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
        {
            if( dependencyObject is Spinner Spinner )
                Spinner.SpinnerForegroundBrush = (Brush)dependencyPropertyChangedEventArgs.NewValue;
        }

        public Brush SpinnerBorderBrush
        {
            get => (Brush)GetValue( SpinnerBorderBrushProperty );
            set
            {
                SetValue( SpinnerBorderBrushProperty, value );
                OnPropertyChanged( nameof( SpinnerBorderBrush ) );
            }
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SpinnerBorderBrushProperty =
            DependencyProperty.Register( nameof( SpinnerBorderBrush ), typeof( Brush ), typeof( Spinner ), new FrameworkPropertyMetadata( Brushes.Silver, FrameworkPropertyMetadataOptions.None, SpinnerBorderPropertyChangedCallback ) );

        private static void SpinnerBorderPropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
        {
            if( dependencyObject is Spinner Spinner )
                Spinner.SpinnerBorderBrush = (Brush)dependencyPropertyChangedEventArgs.NewValue;
        }

        public bool DisableUp
        {
            get => (bool)GetValue( DisableUpProperty );
            set
            {
                SetValue( DisableUpProperty, value );
                OnPropertyChanged( nameof( DisableUp ) );
            }
        }

        // Using a DependencyProperty as the backing store for DisableUp.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DisableUpProperty =
            DependencyProperty.Register( nameof( DisableUp ), typeof( bool ), typeof( Spinner ), new FrameworkPropertyMetadata( false, FrameworkPropertyMetadataOptions.AffectsRender, SpinnerDisableUpPropertyChanged ) );

        private static void SpinnerDisableUpPropertyChanged( DependencyObject source, DependencyPropertyChangedEventArgs e )
        {
            if( source is Spinner Spinner )
            {
                var Enabled = !(bool)e.NewValue;
                Spinner.SpinUp.IsEnabled = Enabled;
                Spinner.SpinUpImage.Opacity = Enabled ? 1 : 0.5;
            }
        }

        public bool DisableDown
        {
            get => (bool)GetValue( DisableDownProperty );
            set
            {
                SetValue( DisableDownProperty, value );
                OnPropertyChanged( nameof( DisableDown ) );
            }
        }

        // Using a DependencyProperty as the backing store for DisableDown.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DisableDownProperty =
            DependencyProperty.Register( nameof( DisableDown ), typeof( bool ), typeof( Spinner ), new FrameworkPropertyMetadata( false, FrameworkPropertyMetadataOptions.AffectsRender, SpinnerDisableDownPropertyChanged ) );

        private static void SpinnerDisableDownPropertyChanged( DependencyObject source, DependencyPropertyChangedEventArgs e )
        {
            if( source is Spinner Spinner )
            {
                var Enabled = !(bool)e.NewValue;
                Spinner.SpinDown.IsEnabled = Enabled;
                Spinner.SpinDownImage.Opacity = Enabled ? 1 : 0.5;
            }
        }

        private void UserControl_MouseWheel( object sender, MouseWheelEventArgs e )
        {
            e.Handled = true;
            if( e.Delta < 0 )
                SpinDown_Click( sender, e );
            else
                SpinUp_Click( sender, e );
        }
    }
}