﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Windows.Data;
using System.Windows.Threading;

namespace IdsControlLibraryV2.Virtualisation
{
    public class AsyncObservableCollection<T> : ObservableCollection<T>
    {
        public override event NotifyCollectionChangedEventHandler CollectionChanged;
        private readonly object SyncLock = new object();

        public AsyncObservableCollection()
        {
            EnableCollectionSynchronization( this, SyncLock );
        }

        protected override void OnCollectionChanged( NotifyCollectionChangedEventArgs e )
        {
            using( BlockReentrancy() )
            {
                var Eh = CollectionChanged;
                if( Eh == null ) return;

                var Dispatcher = ( from NotifyCollectionChangedEventHandler Nh in Eh.GetInvocationList()
                                   let Dpo = Nh.Target as DispatcherObject
                                   where Dpo != null
                                   select Dpo.Dispatcher ).FirstOrDefault();

                if( ( Dispatcher != null ) && ( Dispatcher.CheckAccess() == false ) )
                    Dispatcher.Invoke( DispatcherPriority.DataBind, (Action)( () => OnCollectionChanged( e ) ) );
                else
                {
                    foreach( var Delegate in Eh.GetInvocationList() )
                    {
                        var Nh = (NotifyCollectionChangedEventHandler)Delegate;
                        Nh.Invoke( this, e );
                    }
                }
            }
        }

        private static void EnableCollectionSynchronization( IEnumerable collection, object lockObject )
        {
            var Method = typeof( BindingOperations ).GetMethod( nameof( EnableCollectionSynchronization ),
                                                                new[] { typeof( IEnumerable ), typeof( object ) } );
            Method?.Invoke( null, new[] { collection, lockObject } );
        }

        public void Sort( Comparison<T> comparison )
        {
            var SortableList = new List<T>( this );
            SortableList.Sort( comparison );

            for( var I = 0; I < SortableList.Count; I++ )
                Move( IndexOf( SortableList[ I ] ), I );
        }
    }
}