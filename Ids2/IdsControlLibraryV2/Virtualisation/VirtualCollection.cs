﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IdsControlLibraryV2.Virtualisation
{
    public interface IItemsProvider<T, K>
    {
        IList<T> First( uint count );
        IList<T> First( K key, uint count );
        IList<T> Last( uint count );
        IList<T> Last( K key, uint count );
        IList<T> FindRange( K key, int count ); // + == next, - == prior
        IList<T> FindMatch( K key, int count );
        int CompareKey( K k1, K k2 );
        bool Filter( K filter, T value );
        K Key( T item ); // Returns the key for the item
    }

    public interface IVirtualObservableCollection : IList
    {
        void First( Action callback );
        void First();
        void First( object key, Action callback );
        void First( object key );
        void Last( Action callback );
        void Last();
        void Last( object key, Action callback );
        void Last( object key );
        void FindRange( object key, int count, Action callback ); // + == next, - == prior
        void FindRange( object key, int count ); // + == next, - == prior
        void FindMatch( object key, int count, Action callback );
        void FindMatch( object key, int count );
        object PriorKey( object key );
        object NextKey( object key );
        object Current { get; set; }
        Action<IEnumerable> OnCollectionChanged { get; set; }
        int SelectedIndex { get; set; }
        object SelectedKey { get; set; }
    }

    public class VirtualObservableCollection<T, K> : AsyncObservableCollection<T>, IVirtualObservableCollection
    {
        public const uint PREFETCH = 40;

        private readonly object LockObject = new object();

        private readonly HashSet<K> KeyStore = new HashSet<K>();

        public readonly IItemsProvider<T, K> Provider;
        private readonly uint Prefetch;

        public Func<T, T> OnNewItem = t => t;
#pragma warning disable 109
        public new Action<IEnumerable> OnCollectionChanged { get; set; }
#pragma warning restore 109

        private readonly uint MaxCachedItems;

        private K CurrentKey;

        private static bool KeyIsDefault( K obj )
        {
            return EqualityComparer<K>.Default.Equals( obj, default );
        }

        private static bool ItemIsDefault( T obj )
        {
            return EqualityComparer<T>.Default.Equals( obj, default );
        }

        public VirtualObservableCollection( IItemsProvider<T, K> provider, uint prefetch )
        {
            Provider = provider;
            Prefetch = prefetch;
            MaxCachedItems = 3 * prefetch;

            // ReSharper disable once VirtualMemberCallInConstructor
            CollectionChanged += ( sender, args ) => { OnCollectionChanged?.Invoke( this ); };
        }

        public VirtualObservableCollection( IItemsProvider<T, K> provider ) : this( provider, PREFETCH )
        {
        }

        // Don't clear (reduce display flicker)
        private void DoDisplayFilter( IEnumerable<T> items )
        {
            lock( LockObject )
            {
                var ToRemove = new List<int>();
                var ToDisplay = new List<T>();


                // Find Items To Remove
                for( var Index = 0; Index < Count; Index++ )
                {
                    var T = this[ Index ];
                    if( !KeyStore.Contains( Provider.Key( T ) ) )
                        ToRemove.Add( Index );
                }

                // Find Items To Add
                foreach( var T in items )
                {
                    var Key = Provider.Key( T );

                    var Found = false;

                    for( var Index = 0; Index < Count; Index++ )
                    {
                        var T1 = this[ Index ];

                        if( Provider.CompareKey( Provider.Key( T1 ), Key ) == 0 )
                        {
                            Found = true;
                            break;
                        }
                    }

                    if( !Found )
                        ToDisplay.Add( T );
                }

                ToRemove.Reverse();
                foreach( var I in ToRemove )
                    RemoveAt( I );

                // Insert Accending Order
                foreach( var T in ToDisplay )
                {
                    var Key = Provider.Key( T );

                    var Found = false;

                    for( var Index = 0; Index < Count; Index++ )
                    {
                        var T1 = this[ Index ];
                        if( Provider.CompareKey( Key, Provider.Key( T1 ) ) < 0 )
                        {
                            Insert( Index, T );
                            Found = true;
                            break;
                        }
                    }
                    if( !Found )
                        base.Add( T ); // Append To The End
                }

                if( ( Count > MaxCachedItems ) && !KeyIsDefault( CurrentKey ) )
                {
                    var Offset = -1;

                    for( var Index = 0; Index < Count; Index++ )
                    {
                        var T = this[ Index ];
                        if( Provider.CompareKey( CurrentKey, Provider.Key( T ) ) == 0 )
                        {
                            Offset = Index;
                            break;
                        }
                    }

                    if( Offset >= 0 )
                    {
                        var High = Offset + Prefetch;
                        var Low = Offset - Prefetch;
                        if( SelectedIndex < Low )
                        {
                            for( var I = Count - 1; I > High; I-- )
                                RemoveAt( I );
                        }
                        else if( SelectedIndex > High )
                        {
                            for( var I = 0; I < Low; I++ )
                                RemoveAt( 0 );
                        }
                    }
                }
            }
        }

        private void InternalClear()
        {
            KeyStore.Clear();
        }

        public new void Clear()
        {
            lock( LockObject )
                InternalClear();
        }

        private void InternalAdd( IList<T> items )
        {
            CurrentKey = items.Count > 0 ? Provider.Key( items[ 0 ] ) : default;

            var AddList = new List<T>();
            foreach( var Itm in items )
            {
                AddList.Add( OnNewItem( Itm ) ); // Allow user to adjust the item
                KeyStore.Add( Provider.Key( Itm ) );
            }
            DoDisplayFilter( AddList );
        }

        private void InternalClearAndAdd( IList<T> items )
        {
            InternalClear();
            InternalAdd( items );
        }

        public new void Add( T item )
        {
            lock( LockObject )
                InternalAdd( new[] { item } );
        }

        public void First( Action callback )
        {
            Task.Run( () =>
            {
                var Itms = Provider.First( Prefetch );

                lock( LockObject )
                    InternalClearAndAdd( Itms );
                callback?.Invoke();
            } );
        }

        public void First()
        {
            First( null );
        }

        public void First( object key, Action callback )
        {
            Task.Run( () =>
            {
                var Itms = Provider.First( (K)key, Prefetch );
                lock( LockObject )
                    InternalClearAndAdd( Itms );
                callback?.Invoke();
            } );
        }

        public void First( object key )
        {
            First( key, null );
        }

        public void Last( Action callback )
        {
            Task.Run( () =>
            {
                var Itms = Provider.Last( Prefetch );
                lock( LockObject )
                    InternalClearAndAdd( Itms );
                callback?.Invoke();
            } );
        }

        public void Last()
        {
            Last( null );
        }

        public void Last( object key, Action callback )
        {
            Task.Run( () =>
            {
                var Itms = Provider.Last( (K)key, Prefetch );
                lock( LockObject )
                    InternalClearAndAdd( Itms );
                callback?.Invoke();
            } );
        }

        public void Last( object key )
        {
            Last( key, null );
        }

        public void FindRange( object key, int count, Action callback )
        {
            Task.Run( () =>
            {
                var Itms = Provider.FindRange( (K)key, count );

                lock( LockObject )
                    InternalAdd( Itms );

                callback?.Invoke();
            } );
        }

        public void FindRange( object key, int count )
        {
            FindRange( key, count, null );
        }

        public void FindMatch( object key, int count, Action callback )
        {
            Task.Run( () =>
            {
                var Itms = Provider.FindMatch( (K)key, count );

                lock( LockObject )
                {
                    InternalClearAndAdd( Itms );
                    _Current = Itms.Count > 0 ? Itms[ 0 ] : default;
                }
                callback?.Invoke();
            } );
        }

        public void FindMatch( object key, int count )
        {
            FindMatch( key, count, null );
        }

        public object PriorKey( object key )
        {
            K Prev = default;
            var Key = (K)key;

            lock( LockObject )
            {
                foreach( var T in this )
                {
                    var K1 = Provider.Key( T );

                    if( Provider.CompareKey( K1, (K)key ) == 0 )
                        break;
                    Prev = K1;
                }

                K GetPrior()
                {
                    var Match = Provider.FindMatch( Key, -(int)Prefetch );
                    lock( LockObject )
                        InternalAdd( Match );

                    var C = Match.Count;
                    return C > 1 ? Provider.Key( Match[ C - 1 ] ) : default;
                }

                if( Prev == null ) // Need More NOW
                    Prev = GetPrior();
                else
                    Task.Run( () => GetPrior() );
            }
            return Prev;
        }

        public object NextKey( object key )
        {
            var Key = (K)key;
            K Next = default;

            lock( LockObject )
            {
                var NeedNext = false;
                foreach( var T in this )
                {
                    var ItemKey = Provider.Key( T );
                    if( NeedNext )
                    {
                        Next = ItemKey;
                        break;
                    }

                    NeedNext = Provider.CompareKey( Key, ItemKey ) == 0;
                }
            }

            K GetNext()
            {
                var Match = Provider.FindMatch( Key, (int)Prefetch );
                lock( LockObject )
                    InternalAdd( Match );
                return Match.Count > 1 ? Provider.Key( Match[ 0 ] ) : default;
            }

            if( Next == null ) // Need More NOW
                Next = GetNext();
            else
                Task.Run( () => GetNext() );

            return Next;
        }

        private T _Current;

        public T SelectedItem { get; private set; }
        private readonly object SelectedLock = new object();

        public K SelectedKey
        {
            get
            {
                lock( SelectedLock )
                    return SelectedItem != null ? Provider.Key( SelectedItem ) : default;
            }

            set
            {
                lock( SelectedLock )
                {
                    int DoMatch()
                    {
                        var Match = -1;

                        for( var Index = 0; Index < Count; Index++ )
                        {
                            var Item = this[ Index ];
                            var Key = Provider.Key( Item );
                            if( Provider.CompareKey( Key, value ) == 0 )
                            {
                                SelectedItem = Item;
                                Match = Index;
                                break;
                            }
                        }
                        return Match;
                    }

                    var Offset = DoMatch();

                    if( Offset >= 0 )
                    {
                        var Low = Count / 3;
                        var High = Count * 2 / 3;
                        if( Offset < Low )
                            FindRange( value, -(int)Prefetch, () => { DoMatch(); } );

                        else
                            FindRange( value, (int)Prefetch, () => { DoMatch(); } );
                    }
                }
            }
        }

        private int _SelectedIndex = -1;

        public int SelectedIndex
        {
            get
            {
                lock( SelectedLock )
                    return _SelectedIndex;
            }
            set
            {
                lock( SelectedLock )
                {
                    _SelectedIndex = value;
                    if( value >= 0 )
                        SelectedKey = Provider.Key( this[ value ] );
                    else
                        SelectedItem = default;
                }
            }
        }

        public object Current
        {
            get
            {
                lock( LockObject )
                    return _Current;
            }

            set
            {
                var Key = (K)value;

                lock( LockObject )
                {
                    foreach( var T in this )
                    {
                        if( Provider.CompareKey( Key, Provider.Key( T ) ) == 0 )
                        {
                            _Current = T;
                            return;
                        }
                    }
                    _Current = default;
                }
            }
        }

        object IVirtualObservableCollection.SelectedKey
        {
            get => SelectedKey;
            set => SelectedKey = (K)value;
        }
    }
}