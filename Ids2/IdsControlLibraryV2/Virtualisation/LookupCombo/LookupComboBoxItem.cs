﻿using System.Windows.Media;

namespace IdsControlLibraryV2.Virtualisation.LookupCombo
{
    public interface ILookupComboBoxVirtualObservableCollection : IVirtualObservableCollection
    {
        Brush EvenRowBackground { get; set; }
        Brush OddRowBackground { get; set; }
        Brush EvenRowForeground { get; set; }
        Brush OddRowForeground { get; set; }
    }

    public class LookupComboBoxVirtualObservableCollection<T> : VirtualObservableCollection<T, string>, ILookupComboBoxVirtualObservableCollection
        where T : ComboBoxLookupItem
    {
        public Brush EvenRowBackground { get; set; }
        public Brush OddRowBackground { get; set; }
        public Brush EvenRowForeground { get; set; }
        public Brush OddRowForeground { get; set; }

        public LookupComboBoxVirtualObservableCollection( IItemsProvider<T, string> dataProvider ) : base( dataProvider )
        {
            OnNewItem = item =>
            {
                item.EvenRowBackground = EvenRowBackground;
                item.EvenRowForeground = EvenRowForeground;
                item.OddRowBackground = OddRowBackground;
                item.OddRowForeground = OddRowForeground;
                return item;
            };
        }
    }
}