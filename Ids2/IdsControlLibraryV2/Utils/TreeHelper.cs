﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace IdsControlLibraryV2.Utils
{
    public static class TreeHelper
    {
        public static void RemoveParent( this UIElement element )
        {
            var P = VisualTreeHelper.GetParent( element );
            if( P != null )
            {
                switch( P )
                {
                case Panel Panel:
                    Panel.Children.Remove( element );
                    break;
                case Decorator Decorator when Equals( Decorator.Child, element ):
                    Decorator.Child = null;
                    break;
                case ContentPresenter ContentPresenter when Equals( ContentPresenter.Content, element ):
                    ContentPresenter.Content = null;
                    break;
                case ContentControl ContentControl when Equals( ContentControl.Content, element ):
                    ContentControl.Content = null;
                    break;
                }
            }
        }

        public static void AddParent( this UIElement element, DependencyObject newParent )
        {
            if( newParent != null )
            {
                switch( newParent )
                {
                case Panel Panel:
                    var Children = Panel.Children;
                    if( !Children.Contains( element ) )
                    {
                        element.RemoveParent();
                        Children.Add( element );
                    }
                    break;
                case Decorator Decorator:
                    Decorator.Child = element;
                    break;
                case ContentPresenter ContentPresenter:
                    ContentPresenter.Content = element;
                    break;
                case ContentControl ContentControl:
                    ContentControl.Content = element;
                    break;
                }
            }
        }

        public static T FindParent<T>( this DependencyObject child ) where T : DependencyObject
        {
            if( child != null )
            {
                while( true )
                {
                    //get parent item
                    var ParentObject = VisualTreeHelper.GetParent( child );

                    //we've reached the end of the tree
                    switch( ParentObject )
                    {
                    case null:
                        return null;
                    case T Parent:
                        return Parent;
                    }

                    //check if the parent matches the type we're looking for
                    child = ParentObject;
                }
            }
            return null;
        }

        public static T FindChild<T>( this DependencyObject depObj ) where T : DependencyObject
        {
            if( depObj == null )
                return null;

            var Count = VisualTreeHelper.GetChildrenCount( depObj );

            for( var I = 0; I < Count; I++ )
            {
                var Child = VisualTreeHelper.GetChild( depObj, I );

                var Result = Child as T ?? FindChild<T>( Child );
                if( Result != null )
                    return Result;
            }
            return null;
        }

        public static DependencyObject FindParentOfChild( this DependencyObject depObj, DependencyObject childToFind )
        {
            var Children = depObj.FindChildren<DependencyObject>();
            if( Children != null )
            {
                return ( from Child in Children
                         where Equals( Child, childToFind )
                         select Child.FindParent<DependencyObject>() ).FirstOrDefault();
            }
            return null;
        }

        public static List<T> FindChildren<T>( this DependencyObject depObj ) where T : DependencyObject
        {
            var RetVal = new List<T>();

            if( depObj != null )
            {
                var Count = VisualTreeHelper.GetChildrenCount( depObj );

                for( var I = 0; I < Count; I++ )
                {
                    var Child = VisualTreeHelper.GetChild( depObj, I );

                    if( Child is T Result )
                        RetVal.Add( Result );

                    RetVal.AddRange( FindChildren<T>( Child ) );
                }
            }
            return RetVal;
        }
    }
}