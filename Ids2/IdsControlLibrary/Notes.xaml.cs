﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using IdsControlLibrary.Annotations;
using IdsControlLibrary.Utils;

namespace IdsControlLibrary
{
    /// <summary>
    ///     Interaction logic for Notes.xaml
    /// </summary>
    public partial class Notes : INotifyPropertyChanged
    {
        private const string ITEM_SOURCE = "ItemSource";
        private const string NEW_NOTE_TEXT = "NewNoteText";

        public delegate void NewNoteEvent( string note );

        public delegate void DeleteNoteEvent( int index );

        public delegate void EditNoteEvent( int index, string text );

        public Notes()
        {
            InitializeComponent();
        }

        public IList<string> ItemSource
        {
            get => (IList<string>)GetValue( ItemSourceProperty );
            set => SetValue( ItemSourceProperty, value );
        }

        public string NewNoteText
        {
            get => (string)GetValue( NewNoteTextProperty );
            set => SetValue( NewNoteTextProperty, value );
        }

        // Using a DependencyProperty as the backing store for ItemSource.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ItemSourceProperty =
            DependencyProperty.Register( ITEM_SOURCE, typeof( IList<string> ), typeof( Notes ) );

        public static readonly DependencyProperty NewNoteTextProperty =
            DependencyProperty.Register( NEW_NOTE_TEXT, typeof( string ), typeof( Notes ), new UIPropertyMetadata( "-- New Note --" ) );

        public int MaxItemHeight
        {
            get => (int)GetValue( MaxItemHeightProperty );
            set => SetValue( MaxItemHeightProperty, value );
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MaxItemHeightProperty =
            DependencyProperty.Register( "MaxItemHeight", typeof( int ), typeof( Notes ), new PropertyMetadata( 50 ) );

        public event PropertyChangedEventHandler PropertyChanged;
        public event NewNoteEvent OnNewNote;
        public event DeleteNoteEvent OnDeleteNote;
        public event EditNoteEvent OnEditNote;

        private bool NewNote, Edited, NeedToClearText;

        private void AddButton_Click( object sender, RoutedEventArgs e )
        {
            NoteTextBox.Text = "";
            NewNote = true;
            NeedToClearText = true;
            NoteTextBox.Text = NewNoteText;
        }

        private void NoteTextBox_TextChanged( object sender, TextChangedEventArgs e )
        {
            Edited = true;
        }

        private void NoteList_SelectionChanged( object sender, SelectionChangedEventArgs e )
        {
            NoteTextBox.Text = NoteList.SelectedValue as string ?? "";
        }

        [ NotifyPropertyChangedInvocator ]
        protected virtual void OnPropertyChanged( [ CallerMemberName ] string propertyName = null )
        {
            if( propertyName == ITEM_SOURCE )
            {
                Edited = false;
                PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
            }
        }

        private void DeleteBtn_Click( object sender, RoutedEventArgs e )
        {
            if( sender is Button Sender )
            {
                var Item = Sender.FindParent<ListBoxItem>();
                if( Item != null )
                {
                    Item.IsSelected = true;
                    var Ndx = NoteList.SelectedIndex;
                    if( Ndx >= 0 )
                    {
                        var Lst = ItemSource;
                        if( Lst != null )
                        {
                            Lst.RemoveAt( Ndx );
                            ItemSource = null;
                            ItemSource = Lst;
                            NoteList.UpdateLayout();

                            OnDeleteNote?.Invoke( Ndx );
                        }
                    }
                }
            }
        }

        private void NoteTextBox_GotFocus( object sender, RoutedEventArgs e )
        {
            if( NeedToClearText )
            {
                NeedToClearText = false;
                NoteTextBox.Text = "";
            }
        }

        private void NoteTextBox_LostFocus( object sender, RoutedEventArgs e )
        {
            var Txt = NoteTextBox.Text.Trim();
            if( !string.IsNullOrEmpty( Txt ) )
            {
                if( NewNote )
                {
                    var Lst = ItemSource;
                    if( Lst != null )
                    {
                        Lst.Insert( 0, Txt );
                        ItemSource = null;
                        ItemSource = Lst;
                        NoteList.UpdateLayout();
                    }
                    OnNewNote?.Invoke( Txt );
                    NewNote = false;
                }
                else if( Edited )
                {
                    var Ndx = NoteList.SelectedIndex;
                    if( Ndx >= 0 )
                    {
                        var Lst = ItemSource;
                        if( Lst != null )
                        {
                            Lst[ Ndx ] = Txt;
                            ItemSource = null;
                            ItemSource = Lst;
                            NoteList.SelectedIndex = Ndx;
                            NoteList.UpdateLayout();
                            OnEditNote?.Invoke( Ndx, Txt );
                        }
                    }
                }
            }
        }
    }
}