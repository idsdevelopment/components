﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace IdsControlLibrary.Utils
{
    public static class TreeHelper
    {
        public static void RemoveParent( this UIElement element )
        {
            var P = VisualTreeHelper.GetParent( element );
            if( P != null )
            {
                if( P is Panel Panel )
                    Panel.Children.Remove( element );
                else if( P is Decorator Decorator && Equals( Decorator.Child, element ) )
                    Decorator.Child = null;
                else if( P is ContentPresenter ContentPresenter && Equals( ContentPresenter.Content, element ) )
                    ContentPresenter.Content = null;
                else if( P is ContentControl ContentControl && Equals( ContentControl.Content, element ) )
                    ContentControl.Content = null;
            }
        }

        public static void AddParent( this UIElement element, DependencyObject newParent )
        {
            if( newParent != null )
            {
                if( newParent is Panel Panel )
                {
                    var Children = Panel.Children;
                    if( !Children.Contains( element ) )
                    {
                        element.RemoveParent();
                        Children.Add( element );
                    }
                }
                else if( newParent is Decorator Decorator )
                    Decorator.Child = element;
                else if( newParent is ContentPresenter ContentPresenter )
                    ContentPresenter.Content = element;
                else if( newParent is ContentControl ContentControl )
                    ContentControl.Content = element;
            }
        }

        public static T FindParent<T>( this DependencyObject child ) where T : DependencyObject
        {
            if( child != null )
            {
                while( true )
                {
                    //get parent item
                    var ParentObject = VisualTreeHelper.GetParent( child );

                    //we've reached the end of the tree
                    if( ParentObject == null )
                        return null;

                    //check if the parent matches the type we're looking for
                    if( ParentObject is T Parent )
                        return Parent;
                    child = ParentObject;
                }
            }
            return null;
        }

        public static T FindChild<T>( this DependencyObject depObj ) where T : DependencyObject
        {
            if( depObj == null )
                return null;

            var Count = VisualTreeHelper.GetChildrenCount( depObj );

            for( var I = 0; I < Count; I++ )
            {
                var Child = VisualTreeHelper.GetChild( depObj, I );

                var Result = Child as T ?? FindChild<T>( Child );
                if( Result != null )
                    return Result;
            }
            return null;
        }

        public static DependencyObject FindParentOfChild( this DependencyObject depObj, DependencyObject childToFind )
        {
            var Children = depObj.FindChildren<DependencyObject>();
            if( Children != null )
            {
                return ( from Child in Children
                         where Equals( Child, childToFind )
                         select Child.FindParent<DependencyObject>() ).FirstOrDefault();
            }
            return null;
        }

        public static List<T> FindChildren<T>( this DependencyObject depObj ) where T : DependencyObject
        {
            var RetVal = new List<T>();

            if( depObj != null )
            {
                var Count = VisualTreeHelper.GetChildrenCount( depObj );

                for( var I = 0; I < Count; I++ )
                {
                    var Child = VisualTreeHelper.GetChild( depObj, I );

                    if( Child is T Result )
                        RetVal.Add( Result );

                    RetVal.AddRange( FindChildren<T>( Child ) );
                }
            }
            return RetVal;
        }
    }
}