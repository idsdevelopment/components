﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using IdsControlLibrary.Properties;
using IdsControlLibrary.Utils;
using IdsUpdater;

namespace IdsControlLibrary
{
    internal class TabMenu
    {
        private ObservableCollection<TabItem> _Menu;

        internal ObservableCollection<TabItem> Menu
        {
            get => new ObservableCollection<TabItem>( _Menu );
            private set => _Menu = value;
        }

        private static List<TabItem> MakeCollection( IdsContentPresenter presenter, Page page, IEnumerable<TabbarItem> tabbarItems )
        {
            var Items = new List<TabItem>();

            foreach( var Item in tabbarItems )
            {
                Item.Presenter = presenter;
                Item.Page = page;
                Item.RemoveParent();

                Items.Add( new TabItem
                           {
                               Header = Item.Header,
                               Content = Item.ToolBarContent,
                               Tag = page
                           } );
            }
            return Items;
        }

        private static IEnumerable<List<TabItem>> MakeCollection( IdsContentPresenter presenter, IEnumerable<Page> pages )
        {
            var Result = new List<List<TabItem>>();

            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach( var P in pages )
            {
                if( P.Content is FrameworkElement Fe )
                {
                    var TabBars = Fe.FindChildren<TabbarItem>();
                    Result.Add( MakeCollection( presenter, P, TabBars ) );
                }
            }
            return Result;
        }

        internal TabMenu( IdsContentPresenter presenter, IEnumerable<Page> pages )
        {
            var Hamburger = new TabItem
                            {
                                Header = "☰",
                                FontFamily = new FontFamily( "Segoe UI Symbol" ),
                                FontSize = 24,
                                RenderTransformOrigin = new Point( 0.5, 0.5 ),
                                MinHeight = 40,
                                Background = presenter.TitleBarBackground,
                                Foreground = presenter.TabForeground,
                                Tag = -1,
                                Margin = new Thickness( 0, -5, 0, 0 ),
                                Focusable = false
                            };

            Hamburger.MouseEnter += presenter.Border_MouseEnter;
            Hamburger.MouseLeave += presenter.Border_MouseLeave;
            Hamburger.PreviewMouseDown += ( sender, args ) => { presenter.DoHamburger(); };

            var M = new ObservableCollection<TabItem>
                    {
                        Hamburger
                    };

            var Temp = MakeCollection( presenter, pages );

            foreach( var PageItems in Temp )
            {
                foreach( var PageItem in PageItems )
                    M.Add( PageItem );
            }

            Menu = M;
        }
    }

    /// <summary>
    ///     Interaction logic for IdsContentPresenter.xaml
    /// </summary>
    public partial class IdsContentPresenter
    {
        private Window MainWindow;
        private HamburgerWindow HamburgerWindow;

        private readonly bool DesignMode;

        private TabMenu PrimaryTabMenu, AlternateTabMenu;

        public IdsContentPresenter()
        {
            InitializeComponent();
            DesignMode = DesignerProperties.GetIsInDesignMode( this );
            if( !DesignMode )
                UpdateAvailable.Visibility = Visibility.Collapsed;
        }

        public void LoadProgram( Page program )
        {
            Dispatcher.Invoke( () =>
            {
                void ShowProgram()
                {
                    var P = program;
                    PageFrame.Content = P;
                    P.Visibility = Visibility.Visible;
                    P.FadeIn();
                }

                ShowProgram();
            } );
        }

        internal void Show( TabbarItem item, Action onShown )
        {
            Dispatcher.Invoke( () =>
            {
                var P = item.Page;
                PageFrame.Content = P;
                P.Visibility = Visibility.Visible;
                P.FadeIn( onShown );
            } );
        }

        private bool Swapping;

        public void SwapPageSets()
        {
            Swapping = true;
            Dispatcher.Invoke( () =>
            {
                PageFrame.Content = null;

                var TempM = PrimaryTabMenu;
                PrimaryTabMenu = AlternateTabMenu;
                AlternateTabMenu = TempM;

                var Temp = Pages;
                Pages = AlternatePages;
                AlternatePages = Temp;

                MainTabControl.ItemsSource = PrimaryTabMenu.Menu;

                MainTabControl.SelectedIndex = 1;
                MainTabControl.UpdateLayout();
            } );

            Swapping = false;
        }

        private Updater Updater;

        public void CheckForUpdates( uint currentVersion, string downloadFolder, string exeName )
        {
            Updater = new Updater( currentVersion, downloadFolder, exeName, ( updater, newVersionNumber ) => { Dispatcher.InvokeAsync( () => { UpdateAvailable.Visibility = Visibility.Visible; } ); } );
        }

        public ImageSource Icon
        {
            get => (ImageSource)GetValue( IconProperty );
            set => SetValue( IconProperty, value );
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconProperty =
            DependencyProperty.Register( "Icon", typeof( ImageSource ), typeof( IdsContentPresenter ) );

        public string Title
        {
            get => (string)GetValue( TitleProperty );
            set => SetValue( TitleProperty, value );
        }

        // Using a DependencyProperty as the backing store for Title.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TitleProperty =
            DependencyProperty.Register( "Title", typeof( string ), typeof( IdsContentPresenter ), new PropertyMetadata( "" ) );

        public Brush TitleForeground
        {
            get => (Brush)GetValue( TitleForegroundProperty );
            set => SetValue( TitleForegroundProperty, value );
        }

        // Using a DependencyProperty as the backing store for TitleForeground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TitleForegroundProperty =
            DependencyProperty.Register( "TitleForeground", typeof( Brush ), typeof( IdsContentPresenter ), new PropertyMetadata( Brushes.Black ) );

        public Brush TitleBackground
        {
            get => (Brush)GetValue( TitleBackgroundProperty );
            set => SetValue( TitleBackgroundProperty, value );
        }

        // Using a DependencyProperty as the backing store for TitleBackground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TitleBackgroundProperty =
            DependencyProperty.Register( "TitleBackground", typeof( Brush ), typeof( IdsContentPresenter ), new PropertyMetadata( Brushes.Transparent ) );

        public Brush TitleBarBackground
        {
            get => (Brush)GetValue( TitleBarBackgroundProperty );
            set => SetValue( TitleBarBackgroundProperty, value );
        }

        // Using a DependencyProperty as the backing store for TitleBarBackground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TitleBarBackgroundProperty =
            DependencyProperty.Register( "TitleBarBackground", typeof( Brush ), typeof( IdsContentPresenter ), new PropertyMetadata( Brushes.Transparent ) );

        public Brush TabForeground
        {
            get => (Brush)GetValue( TabForegroundProperty );
            set => SetValue( TabForegroundProperty, value );
        }

        // Using a DependencyProperty as the backing store for TabForeground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TabForegroundProperty =
            DependencyProperty.Register( "TabForeground", typeof( Brush ), typeof( IdsContentPresenter ), new PropertyMetadata( Brushes.AliceBlue ) );

        public Brush TabSelectedBackground
        {
            get => (Brush)GetValue( TabSelectedBackgroundProperty );
            set => SetValue( TabSelectedBackgroundProperty, value );
        }

        // Using a DependencyProperty as the backing store for TabSelectedBackground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TabSelectedBackgroundProperty =
            DependencyProperty.Register( "TabSelectedBackground", typeof( Brush ), typeof( IdsContentPresenter ), new PropertyMetadata( Brushes.White ) );

        public FrameworkElement TitleBarContent
        {
            get => (FrameworkElement)GetValue( TitleBarContentProperty );
            set => SetValue( TitleBarContentProperty, value );
        }

        // Using a DependencyProperty as the backing store for TitleBarContent.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TitleBarContentProperty =
            DependencyProperty.Register( "TitleBarContent", typeof( FrameworkElement ), typeof( IdsContentPresenter ) );

        public List<Page> Pages
        {
            get => (List<Page>)GetValue( PagesProperty );
            set
            {
                SetValue( PagesProperty, value );
                if( value != null )
                {
                    if( !Swapping )
                        PrimaryTabMenu = new TabMenu( this, value );

                    MainTabControl.ItemsSource = PrimaryTabMenu.Menu;
                    MainTabControl.SelectedIndex = 1;
                }
                else
                    MainTabControl.SelectedIndex = -1;
            }
        }

        // Using a DependencyProperty as the backing store for Pages.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PagesProperty =
            DependencyProperty.Register( "Pages", typeof( List<Page> ), typeof( IdsContentPresenter ), new PropertyMetadata( new List<Page>() ) );

        public List<Page> AlternatePages
        {
            get => (List<Page>)GetValue( AlternatePagesProperty );
            set
            {
                SetValue( AlternatePagesProperty, value );
                if( !Swapping )
                    AlternateTabMenu = new TabMenu( this, value );
            }
        }

        // Using a DependencyProperty as the backing store for Pages.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AlternatePagesProperty =
            DependencyProperty.Register( "AlternatePages", typeof( List<Page> ), typeof( IdsContentPresenter ), new PropertyMetadata( new List<Page>() ) );

        public Brush HamburgerBackground
        {
            get => (Brush)GetValue( HamburgerBackgroundProperty );
            set => SetValue( HamburgerBackgroundProperty, value );
        }

        // Using a DependencyProperty as the backing store for HamburgerBackground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HamburgerBackgroundProperty =
            DependencyProperty.Register( "HamburgerBackground", typeof( Brush ), typeof( IdsContentPresenter ), new PropertyMetadata( Brushes.DarkGray ) );

        public Brush HamburgerForeground
        {
            get => (Brush)GetValue( HamburgerForegroundProperty );
            set => SetValue( HamburgerForegroundProperty, value );
        }

        // Using a DependencyProperty as the backing store for HamburgerForeground.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HamburgerForegroundProperty =
            DependencyProperty.Register( "HamburgerForeground", typeof( Brush ), typeof( IdsContentPresenter ), new PropertyMetadata( Brushes.White ) );

        public HamburgerContent HamburgerContent
        {
            get => (HamburgerContent)GetValue( HamburgerContentProperty );
            set
            {
                SetValue( HamburgerContentProperty, value );

                var Items = value.Items;

                if( ( HamburgerWindow != null ) && ( Items != null ) )
                {
                    HamburgerWindow.Background = HamburgerBackground;
                    HamburgerWindow.Foreground = HamburgerForeground;
                    HamburgerWindow.ListBox.ItemsSource = Items;
                    HamburgerWindow.ListBox.SelectedIndex = -1;
                }
            }
        }

        // Using a DependencyProperty as the backing store for HamburgerContent.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HamburgerContentProperty =
            DependencyProperty.Register( "HamburgerContent", typeof( HamburgerContent ), typeof( IdsContentPresenter ) );

        public double HamburgerWidth
        {
            get => (double)GetValue( HamburgerWidthProperty );
            set => SetValue( HamburgerWidthProperty, value );
        }

        // Using a DependencyProperty as the backing store for HamburgerWidth.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HamburgerWidthProperty =
            DependencyProperty.Register( "HamburgerWidth", typeof( double ), typeof( IdsContentPresenter ), new PropertyMetadata( 300.0 ) );

        private void CloseButton_Click( object sender, RoutedEventArgs e )
        {
            MainWindow.Close();
            Application.Current.Shutdown();
        }

        private void MaximiseButton_Click( object sender, RoutedEventArgs e )
        {
            MainWindow.WindowState = MainWindow.WindowState == WindowState.Maximized ? WindowState.Normal : WindowState.Maximized;
        }

        private void MinimiseButton_Click( object sender, RoutedEventArgs e )
        {
            MainWindow.WindowState = WindowState.Minimized;
        }

        public double Scaling { get; private set; }

        private void UserControl_Loaded( object sender, RoutedEventArgs e )
        {
            if( !DesignMode )
            {
                MainWindow = this.FindParent<Window>();
                MainWindow.WindowStyle = WindowStyle.None;
                MainWindow.Background = Brushes.Transparent;
                MainWindow.ResizeMode = ResizeMode.CanResizeWithGrip;
                HamburgerWindow = new HamburgerWindow( MainWindow, this )
                                  {
                                      Visibility = Visibility.Hidden,
                                      Width = HamburgerWidth,
                                      Background = HamburgerBackground,
                                      Foreground = HamburgerForeground,
                                      ListBox = { ItemsSource = new List<HamburgerItem>() }
                                  };
            }

            // Force update
            Pages = Pages;
            AlternatePages = AlternatePages;

            MainTabControl_SelectionChanged( sender, null );
            Scaling = Settings.Default.DisplayScaling;
            DisplayScaleSlider.Value = Scaling;
            MainLayoutPanel.LayoutTransform = new ScaleTransform( Scaling, Scaling );

            Initialised = true;
        }

        private Timer MouseDragTimer, DoubleClickTimer;

        public void CancelDragMover()
        {
            if( MouseDragTimer != null )
            {
                var Mdt = MouseDragTimer;
                MouseDragTimer = null;
                Mdt.Stop();
                Mdt.Dispose();
            }
        }

        private bool _DisableDrag;

        public bool DisableDrag
        {
            get => _DisableDrag;
            set
            {
                _DisableDrag = value;
                if( MouseDragTimer != null )
                {
                    MouseDragTimer.Stop();
                    MouseDragTimer.Dispose();
                    MouseDragTimer = null;
                }
                CancelDragMover();
            }
        }

        private void DockPanel_MouseDown( object sender, MouseButtonEventArgs e )
        {
            if( e.ChangedButton == MouseButton.Left )
            {
                if( DoubleClickTimer == null )
                {
                    DoubleClickTimer = new Timer( 300 );
                    DoubleClickTimer.Elapsed += ( o, args ) =>
                    {
                        var Tmr = DoubleClickTimer;
                        DoubleClickTimer = null;
                        Tmr.Stop();
                        Tmr.Dispose();
                    };
                    DoubleClickTimer.Start();
                }
                else
                {
                    MainWindow.WindowState = MainWindow.WindowState == WindowState.Normal ? WindowState.Maximized : WindowState.Normal;
                    return;
                }

                if( !DisableDrag && ( MouseDragTimer == null ) )
                {
                    MouseDragTimer = new Timer( 300 );
                    MouseDragTimer.Elapsed += ( o, args ) =>
                    {
                        CancelDragMover();
                        Dispatcher.Invoke( () =>
                        {
                            try
                            {
                                MainWindow.DragMove();
                            }
                            catch
                            {
                            }
                        } );
                    };
                    MouseDragTimer.Start();
                }
            }
        }

        private void DockPanel_MouseUp( object sender, MouseButtonEventArgs e )
        {
            CancelDragMover();
        }

        internal void DoHamburger()
        {
            if( Initialised )
                HamburgerWindow?.DoHamburgerClick();
        }

        private void MainTabControl_SelectionChanged( object sender, SelectionChangedEventArgs e )
        {
            if( e != null )
            {
                e.Handled = true;

                Border_MouseLeave( sender, null );

                var Selected = MainTabControl.SelectedIndex;
                if( Selected > 0 )
                {
                    var Ndx = 0;
                    foreach( var Item in MainTabControl.Items )
                    {
                        if( Item is TabItem TbItem )
                        {
                            var N = Ndx++;
                            if( ( N != 0 ) && ( N == Selected ) )
                            {
                                TbItem.Foreground = TitleBarBackground;
                                TbItem.Background = TabSelectedBackground;
                            }
                            else
                            {
                                TbItem.Background = TitleBarBackground;
                                TbItem.Foreground = TabForeground;
                            }
                        }
                    }
                }
            }
        }

        private TabItem UnselectedItem;

        internal void Border_MouseEnter( object sender, MouseEventArgs e )
        {
            if( UnselectedItem == null )
            {
                e.Handled = true;

                TabItem Item;

                if( sender is Border Border )
                    Item = Border.FindParent<TabItem>();
                else
                    Item = (TabItem)sender;

                if( !Equals( Item.Background, TabSelectedBackground ) )
                {
                    if( !( Item.Tag is int ) )
                    {
                        UnselectedItem = Item;

                        Item.Background = TabSelectedBackground.Clone();
                        Item.Background.Opacity = 0.2;
                        Item.Foreground = TabForeground.Clone();
                    }
                }
            }
        }

        internal void Border_MouseLeave( object sender, MouseEventArgs e )
        {
            if( UnselectedItem != null )
            {
                var Item = UnselectedItem;
                UnselectedItem = null;
                Item.Background = TitleBarBackground;
                Item.Foreground = TabForeground;
                Item.Opacity = 1;
            }
        }

        private void UpdateAvailable_Click( object sender, RoutedEventArgs e )
        {
            Updater.RunInstall();
            Application.Current.Shutdown();
        }

        private bool Initialised;

        private void Slider_ValueChanged( object sender, RoutedPropertyChangedEventArgs<double> e )
        {
            if( Initialised && sender is Slider Slider )
            {
                var Scale = Slider.Value;
                Scaling = Scale;
                var Setting = Settings.Default;
                Setting.DisplayScaling = Scale;
                Setting.Save();
                MainLayoutPanel.LayoutTransform = new ScaleTransform( Scale, Scale );
            }
        }
    }
}