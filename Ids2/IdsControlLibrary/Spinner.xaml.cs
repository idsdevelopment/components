﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace IdsControlLibrary
{
    /// <summary>
    ///     Interaction logic for Spinner.xaml
    /// </summary>
    public partial class Spinner
    {
        public Spinner()
        {
            InitializeComponent();
        }

        public event EventHandler SpinUpClick, SpinDownClick;

        private void SpinUp_Click( object sender, RoutedEventArgs e )
        {
            e.Handled = true;
            SpinUpClick?.Invoke( this, e );
        }

        private void SpinDown_Click( object sender, RoutedEventArgs e )
        {
            e.Handled = true;
            SpinDownClick?.Invoke( this, e );
        }

        public Brush SpinnerBackgroundBrush
        {
            get => (Brush)GetValue( SpinnerBackgroundBrushProperty );
            set => SetValue( SpinnerBackgroundBrushProperty, value );
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SpinnerBackgroundBrushProperty =
            DependencyProperty.Register( "SpinnerBackgroundBrush", typeof( Brush ), typeof( Spinner ), new FrameworkPropertyMetadata( Brushes.White, FrameworkPropertyMetadataOptions.None, SpinnerBackgroundPropertyChangedCallback ) );

        private static void SpinnerBackgroundPropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
        {
            if( dependencyObject is Spinner Spinner )
                Spinner.SpinnerBackgroundBrush = (Brush)dependencyPropertyChangedEventArgs.NewValue;
        }

        public Brush SpinnerForegroundBrush
        {
            get => (Brush)GetValue( SpinnerForegroundBrushProperty );
            set => SetValue( SpinnerForegroundBrushProperty, value );
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SpinnerForegroundBrushProperty =
            DependencyProperty.Register( "SpinnerForegroundBrush", typeof( Brush ), typeof( Spinner ), new FrameworkPropertyMetadata( Brushes.Black, FrameworkPropertyMetadataOptions.None, SpinnerForegroundPropertyChangedCallback ) );

        private static void SpinnerForegroundPropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
        {
            if( dependencyObject is Spinner Spinner )
                Spinner.SpinnerForegroundBrush = (Brush)dependencyPropertyChangedEventArgs.NewValue;
        }

        public Brush SpinnerBorderBrush
        {
            get => (Brush)GetValue( SpinnerBorderBrushProperty );
            set => SetValue( SpinnerBorderBrushProperty, value );
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SpinnerBorderBrushProperty =
            DependencyProperty.Register( "SpinnerBorderBrush", typeof( Brush ), typeof( Spinner ), new FrameworkPropertyMetadata( Brushes.Silver, FrameworkPropertyMetadataOptions.None, SpinnerBorderPropertyChangedCallback ) );

        private static void SpinnerBorderPropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
        {
            if( dependencyObject is Spinner Spinner )
                Spinner.SpinnerBorderBrush = (Brush)dependencyPropertyChangedEventArgs.NewValue;
        }

        public bool DisableUp
        {
            get => (bool)GetValue( DisableUpProperty );
            set
            {
                SetValue( DisableUpProperty, value );
                if( value )
                {
                    SpinUp.IsEnabled = false;
                    SpinUpImage.Opacity = 0.5;
                }
                else
                {
                    SpinUp.IsEnabled = true;
                    SpinUpImage.Opacity = 1;
                }
            }
        }

        // Using a DependencyProperty as the backing store for DisableUp.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DisableUpProperty =
            DependencyProperty.Register( "DisableUp", typeof( bool ), typeof( Spinner ), new FrameworkPropertyMetadata( false, FrameworkPropertyMetadataOptions.AffectsRender, SpinnerDisableUpPropertyChanged ) );

        private static void SpinnerDisableUpPropertyChanged( DependencyObject source, DependencyPropertyChangedEventArgs e )
        {
            if( source is Spinner Spinner )
                Spinner.SpinUp.IsEnabled = !(bool)e.NewValue;
        }

        public bool DisableDown
        {
            get => (bool)GetValue( DisableDownProperty );
            set
            {
                SetValue( DisableDownProperty, value );
                if( value )
                {
                    SpinDown.IsEnabled = false;
                    SpinDownImage.Opacity = 0.5;
                }
                else
                {
                    SpinDown.IsEnabled = true;
                    SpinDownImage.Opacity = 1;
                }
            }
        }

        // Using a DependencyProperty as the backing store for DisableDown.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DisableDownProperty =
            DependencyProperty.Register( "DisableDown", typeof( bool ), typeof( Spinner ), new FrameworkPropertyMetadata( false, FrameworkPropertyMetadataOptions.AffectsRender, SpinnerDisableDownPropertyChanged ) );

        private void UserControl_MouseWheel( object sender, MouseWheelEventArgs e )
        {
            e.Handled = true;
            if( e.Delta < 0 )
                SpinDown_Click( sender, e );
            else
                SpinUp_Click( sender, e );
        }

        private static void SpinnerDisableDownPropertyChanged( DependencyObject source, DependencyPropertyChangedEventArgs e )
        {
            if( source is Spinner Spinner )
                Spinner.SpinDown.IsEnabled = !(bool)e.NewValue;
        }
    }
}