﻿using System;
using System.Collections.Generic;
using System.Linq;
using AzureRemoteService;
using IdsControlLibraryV2;
using IdsControlLibraryV2.Virtualisation;
using IdsControlLibraryV2.Virtualisation.LookupCombo;
using Protocol.Data;

namespace IdsRemoteServiceControlLibraryV2.DataSources.Routes
{
    public class RouteComboBoxLookupItem : ComboBoxLookupItem
    {
        public RouteLookupSummary Item { get; set; }
        public override string LookupKey => Item.RouteName;
    }

    public class RouteLookupDataSource : IItemsProvider<RouteComboBoxLookupItem, string>
    {
        private string CustomerCode = "";

        public int CompareKey( string k1, string k2 )
        {
            return string.CompareOrdinal( k1, k2 );
        }

        public bool Filter( string filter, RouteComboBoxLookupItem value )
        {
            return ( filter == null ) || value.LookupKey.StartsWith( filter, StringComparison.InvariantCultureIgnoreCase );
        }

        public string Key( RouteComboBoxLookupItem item )
        {
            return item.LookupKey;
        }

        private IList<RouteComboBoxLookupItem> GetItems( PreFetch.PREFETCH prefetch, string key, int count )
        {
            using( var Client = new AzureClient() )
            {
                var RetVal = new List<RouteComboBoxLookupItem>();

                var Response = Client.RequestRouteLookup( new RouteLookup
                                                          {
                                                              CustomerCode = CustomerCode,
                                                              Key = key,
                                                              PreFetchCount = count,
                                                              Fetch = prefetch
                                                          } ).Result;
                if( Response != null )
                {
                    RetVal.AddRange( from C in Response
                                     select new RouteComboBoxLookupItem
                                            {
                                                Item = C
                                            } );
                }
                return RetVal;
            }
        }

        public IList<RouteComboBoxLookupItem> FindMatch( string key, int count )
        {
            return GetItems( PreFetch.PREFETCH.FIND_MATCH, key, count );
        }

        public IList<RouteComboBoxLookupItem> FindRange( string key, int count )
        {
            return GetItems( PreFetch.PREFETCH.FIND_RANGE, key, count );
        }

        public IList<RouteComboBoxLookupItem> First( uint count )
        {
            return new List<RouteComboBoxLookupItem>();
        }

        public IList<RouteComboBoxLookupItem> First( string key, uint count )
        {
            CustomerCode = key;
            return GetItems( PreFetch.PREFETCH.FIRST, "", (int)count );
        }

        public IList<RouteComboBoxLookupItem> Last( uint count )
        {
            return new List<RouteComboBoxLookupItem>();
        }

        public IList<RouteComboBoxLookupItem> Last( string key, uint count )
        {
            CustomerCode = key;
            return GetItems( PreFetch.PREFETCH.LAST, "", (int)count );
        }
    }

    public class RouteLookupVirtualObservableCollection : LookupComboBoxVirtualObservableCollection<RouteComboBoxLookupItem>
    {
        public string CustomerCode { get; set; }

        public RouteLookupVirtualObservableCollection() : base( new RouteLookupDataSource() )
        {
        }
    }
}