﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using IdsControlLibraryV2;
using IdsRemoteServiceControlLibraryV2.Annotations;
using IdsRemoteServiceControlLibraryV2.DataSources.Customers;

namespace IdsRemoteServiceControlLibraryV2.Customers
{
    /// <summary>
    ///     Interaction logic for CustomerLookupComboBox.xaml
    /// </summary>
    public partial class CustomerLookupComboBox : INotifyPropertyChanged
    {
        public CustomerLookupComboBox()
        {
            InitializeComponent();
            DataContext = LookupCombo;
        }

        [ Category( "Brush" ) ]
        public Brush OddRowBackground
        {
            get => (Brush)GetValue( OddRowBackgroundProperty );
            set
            {
                SetValue( OddRowBackgroundProperty, value );
                OnPropertyChanged( nameof( OddRowBackground ) );
            }
        }

        // Using a DependencyProperty as the backing store for OddRowBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty OddRowBackgroundProperty =
            DependencyProperty.Register( nameof( OddRowBackground ), typeof( Brush ), typeof( CustomerLookupComboBox ), new FrameworkPropertyMetadata( Brushes.White, OddRowBackgroundPropertyChangedCallback ) );

        private static void OddRowBackgroundPropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
        {
            if( dependencyObject is CustomerLookupComboBox CustCombo && dependencyPropertyChangedEventArgs.NewValue is Brush Brush )
                CustCombo.LookupCombo.OddRowBackground = Brush;
        }

        [ Category( "Brush" ) ]
        public Brush OddRowForeground
        {
            get => (Brush)GetValue( OddRowForegroundProperty );
            set
            {
                SetValue( OddRowForegroundProperty, value );
                OnPropertyChanged( nameof( OddRowForeground ) );
            }
        }

        // Using a DependencyProperty as the backing store for OddRowForegroundBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty OddRowForegroundProperty =
            DependencyProperty.Register( nameof( OddRowForeground ), typeof( Brush ), typeof( CustomerLookupComboBox ), new FrameworkPropertyMetadata( Brushes.Black, OddRowForegroundPropertyChangedCallback ) );

        private static void OddRowForegroundPropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
        {
            if( dependencyObject is CustomerLookupComboBox CustCombo && dependencyPropertyChangedEventArgs.NewValue is Brush Brush )
                CustCombo.LookupCombo.OddRowForeground = Brush;
        }

        [ Category( "Brush" ) ]
        public Brush EvenRowBackground
        {
            get => (Brush)GetValue( EvenRowBackgroundProperty );
            set
            {
                SetValue( EvenRowBackgroundProperty, value );
                OnPropertyChanged( nameof( EvenRowBackground ) );
            }
        }

        // Using a DependencyProperty as the backing store for EvenRowBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty EvenRowBackgroundProperty =
            DependencyProperty.Register( nameof( EvenRowBackground ), typeof( Brush ), typeof( CustomerLookupComboBox ), new FrameworkPropertyMetadata( Brushes.LightGray, EvenRowBrushPropertyChangedCallback ) );

        private static void EvenRowBrushPropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
        {
            if( dependencyObject is CustomerLookupComboBox CustCombo && dependencyPropertyChangedEventArgs.NewValue is Brush Brush )
                CustCombo.LookupCombo.EvenRowBackground = Brush;
        }

        [ Category( "Brush" ) ]
        public Brush EvenRowForeground
        {
            get => (Brush)GetValue( EvenRowForegroundProperty );
            set
            {
                SetValue( EvenRowForegroundProperty, value );
                OnPropertyChanged( nameof( EvenRowForeground ) );
            }
        }

        // Using a DependencyProperty as the backing store for EvenRowForegroundBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty EvenRowForegroundProperty =
            DependencyProperty.Register( nameof( EvenRowForeground ), typeof( Brush ), typeof( CustomerLookupComboBox ), new FrameworkPropertyMetadata( Brushes.Black, EvenRowForegroundPropertyChangedCallback ) );

        private static void EvenRowForegroundPropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
        {
            if( dependencyObject is CustomerLookupComboBox CustCombo && dependencyPropertyChangedEventArgs.NewValue is Brush Brush )
                CustCombo.LookupCombo.EvenRowForeground = Brush;
        }

        [ Category( "Common" ) ]
        public int SelectedIndex
        {
            get => (int)GetValue( SelectedIndexProperty );
            set
            {
                SetValue( SelectedIndexProperty, value );
                OnPropertyChanged( nameof( SelectedIndex ) );
            }
        }

        // Using a DependencyProperty as the backing store for SelectedIndex.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedIndexProperty =
            DependencyProperty.Register( nameof( SelectedIndex ), typeof( int ), typeof( CustomerLookupComboBox ), new FrameworkPropertyMetadata( -1 ) );

        [ Category( "Options" ) ]
        public bool EnableNew
        {
            get => (bool)GetValue( EnableNewProperty );
            set
            {
                SetValue( EnableNewProperty, value );
                OnPropertyChanged( nameof( EnableNew ) );
            }
        }

        // Using a DependencyProperty as the backing store for EnableNew.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty EnableNewProperty =
            DependencyProperty.Register( nameof( EnableNew ), typeof( bool ), typeof( CustomerLookupComboBox ), new FrameworkPropertyMetadata( false, EnableNewPropertyChangedCallback ) );

        private static void EnableNewPropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
        {
            if( dependencyObject is CustomerLookupComboBox CustCombo && dependencyPropertyChangedEventArgs.NewValue is bool Enable )
                CustCombo.LookupCombo.EnableNew = Enable;
        }

        [ Category( "Options" ) ]
        public int PreFetch
        {
            get => (int)GetValue( PreFetchProperty );
            set
            {
                SetValue( PreFetchProperty, value );
                OnPropertyChanged( nameof( PreFetch ) );
            }
        }

        // Using a DependencyProperty as the backing store for PreFetch.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PreFetchProperty =
            DependencyProperty.Register( nameof( PreFetch ), typeof( int ), typeof( CustomerLookupComboBox ), new FrameworkPropertyMetadata( LookupComboBoxBase.DEFAULT_PREFETCH, PreFetchPropertyChangedCallback ) );

        private static void PreFetchPropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
        {
            if( dependencyObject is CustomerLookupComboBox CustCombo && dependencyPropertyChangedEventArgs.NewValue is int Count )
                CustCombo.LookupCombo.PreFetch = Count;
        }

        [ Category( "Options" ) ]
        public string CustomerCode
        {
            get => (string)GetValue( CustomerCodeProperty );
            set
            {
                SetValue( CustomerCodeProperty, value );
                OnPropertyChanged( nameof( CustomerCode ) );
            }
        }

        // Using a DependencyProperty as the backing store for CustomerCode.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CustomerCodeProperty =
            DependencyProperty.Register( nameof( CustomerCode ), typeof( string ), typeof( CustomerLookupComboBox ), new FrameworkPropertyMetadata( "", CustomerCodePropertyChangedCallback ) );

        private static void CustomerCodePropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
        {
            if( dependencyObject is CustomerLookupComboBox CustCombo && dependencyPropertyChangedEventArgs.NewValue is string )
                CustCombo.LookupCombo.Text = "";
        }

        public event Action<object, SelectionChangedEventArgs> SelectionChanged;

        public event PropertyChangedEventHandler PropertyChanged;

        [ NotifyPropertyChangedInvocator ]
        protected virtual void OnPropertyChanged( [ CallerMemberName ] string propertyName = null )
        {
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        private void UserControl_Initialized( object sender, EventArgs e )
        {
            LookupCombo.ItemsSource = new CustomerLookupVirtualObservableCollection();
        }

        private void LookupCombo_SelectionChanged( object arg1, SelectionChangedEventArgs arg2 )
        {
            if( ( arg2.AddedItems.Count > 0 ) && arg2.AddedItems[ 0 ] is CustomerComboBoxLookupItem Item )
                CustomerCode = Item.LookupKey;
            OnSelectionChanged( this, arg2 );
        }

        protected virtual void OnSelectionChanged( object arg1, SelectionChangedEventArgs arg2 )
        {
            SelectionChanged?.Invoke( arg1, arg2 );
        }
    }
}