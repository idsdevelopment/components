﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using IdsControlLibraryV2;
using IdsRemoteServiceControlLibraryV2.Annotations;
using IdsRemoteServiceControlLibraryV2.DataSources.Routes;
using Protocol.Data;

namespace IdsRemoteServiceControlLibraryV2.Routes
{
    /// <summary>
    ///     Interaction logic for RouteLookupComboBox.xaml
    /// </summary>
    public partial class RouteLookupComboBox : UserControl
    {
        public RouteLookupComboBox()
        {
            InitializeComponent();
            DataContext = LookupCombo;
        }

        [ Category( "Brush" ) ]
        public Brush OddRowBackground
        {
            get => (Brush)GetValue( OddRowBackgroundProperty );
            set
            {
                SetValue( OddRowBackgroundProperty, value );
                OnPropertyChanged( nameof( OddRowBackground ) );
            }
        }

        // Using a DependencyProperty as the backing store for OddRowBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty OddRowBackgroundProperty =
            DependencyProperty.Register( nameof( OddRowBackground ), typeof( Brush ), typeof( RouteLookupComboBox ), new FrameworkPropertyMetadata( Brushes.White, OddRowBackgroundPropertyChangedCallback ) );

        private static void OddRowBackgroundPropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
        {
            if( dependencyObject is RouteLookupComboBox Combo && dependencyPropertyChangedEventArgs.NewValue is Brush Brush )
                Combo.LookupCombo.OddRowBackground = Brush;
        }

        [ Category( "Brush" ) ]
        public Brush OddRowForeground
        {
            get => (Brush)GetValue( OddRowForegroundProperty );
            set
            {
                SetValue( OddRowForegroundProperty, value );
                OnPropertyChanged( nameof( OddRowForeground ) );
            }
        }

        // Using a DependencyProperty as the backing store for OddRowForegroundBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty OddRowForegroundProperty =
            DependencyProperty.Register( nameof( OddRowForeground ), typeof( Brush ), typeof( RouteLookupComboBox ), new FrameworkPropertyMetadata( Brushes.Black, OddRowForegroundPropertyChangedCallback ) );

        private static void OddRowForegroundPropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
        {
            if( dependencyObject is RouteLookupComboBox Combo && dependencyPropertyChangedEventArgs.NewValue is Brush Brush )
                Combo.LookupCombo.OddRowForeground = Brush;
        }

        [ Category( "Brush" ) ]
        public Brush EvenRowBackground
        {
            get => (Brush)GetValue( EvenRowBackgroundProperty );
            set
            {
                SetValue( EvenRowBackgroundProperty, value );
                OnPropertyChanged( nameof( EvenRowBackground ) );
            }
        }

        // Using a DependencyProperty as the backing store for EvenRowBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty EvenRowBackgroundProperty =
            DependencyProperty.Register( nameof( EvenRowBackground ), typeof( Brush ), typeof( RouteLookupComboBox ), new FrameworkPropertyMetadata( Brushes.LightGray, EvenRowBrushPropertyChangedCallback ) );

        private static void EvenRowBrushPropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
        {
            if( dependencyObject is RouteLookupComboBox Combo && dependencyPropertyChangedEventArgs.NewValue is Brush Brush )
                Combo.LookupCombo.EvenRowBackground = Brush;
        }

        [ Category( "Brush" ) ]
        public Brush EvenRowForeground
        {
            get => (Brush)GetValue( EvenRowForegroundProperty );
            set
            {
                SetValue( EvenRowForegroundProperty, value );
                OnPropertyChanged( nameof( EvenRowForeground ) );
            }
        }

        // Using a DependencyProperty as the backing store for EvenRowForegroundBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty EvenRowForegroundProperty =
            DependencyProperty.Register( nameof( EvenRowForeground ), typeof( Brush ), typeof( RouteLookupComboBox ), new FrameworkPropertyMetadata( Brushes.Black, EvenRowForegroundPropertyChangedCallback ) );

        private static void EvenRowForegroundPropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
        {
            if( dependencyObject is RouteLookupComboBox Combo && dependencyPropertyChangedEventArgs.NewValue is Brush Brush )
                Combo.LookupCombo.EvenRowForeground = Brush;
        }

        [ Category( "Common" ) ]
        public int SelectedIndex
        {
            get => (int)GetValue( SelectedIndexProperty );
            set
            {
                SetValue( SelectedIndexProperty, value );
                OnPropertyChanged( nameof( SelectedIndex ) );
            }
        }

        // Using a DependencyProperty as the backing store for SelectedIndex.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedIndexProperty =
            DependencyProperty.Register( nameof( SelectedIndex ), typeof( int ), typeof( RouteLookupComboBox ), new PropertyMetadata( -1 ) );

        [ Category( "Options" ) ]
        public int PreFetch
        {
            get => (int)GetValue( PreFetchProperty );
            set
            {
                SetValue( PreFetchProperty, value );
                OnPropertyChanged( nameof( PreFetch ) );
            }
        }

        // Using a DependencyProperty as the backing store for PreFetch.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PreFetchProperty =
            DependencyProperty.Register( nameof( PreFetch ), typeof( int ), typeof( RouteLookupComboBox ), new FrameworkPropertyMetadata( LookupComboBoxBase.DEFAULT_PREFETCH, PreFetchPropertyChangedCallback ) );

        private static void PreFetchPropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
        {
            if( dependencyObject is RouteLookupComboBox Combo && dependencyPropertyChangedEventArgs.NewValue is int Count )
                Combo.LookupCombo.PreFetch = Count;
        }

        [ Category( "Options" ) ]
        public string CustomerCode
        {
            get => (string)GetValue( CustomerCodeProperty );
            set
            {
                SetValue( CustomerCodeProperty, value );
                OnPropertyChanged( nameof( CustomerCode ) );
            }
        }

        // Using a DependencyProperty as the backing store for CustomerCode.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CustomerCodeProperty =
            DependencyProperty.Register( nameof( CustomerCode ), typeof( string ), typeof( RouteLookupComboBox ), new FrameworkPropertyMetadata( "", CustomerCodePropertyChangedCallback ) );

        private static void CustomerCodePropertyChangedCallback( DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs )
        {
            if( dependencyObject is RouteLookupComboBox Combo && dependencyPropertyChangedEventArgs.NewValue is string CustCode )
            {
                var Rc = Combo.RouteCollection;
                Rc.CustomerCode = CustCode;
                Rc.First( CustCode );
            }
        }

        [ Category( "Options" ) ]
        public RouteLookupSummary Route
        {
            get => (RouteLookupSummary)GetValue( RouteProperty );
            private set
            {
                SetValue( RouteProperty, value );
                OnPropertyChanged( nameof( Route ) );
            }
        }

        // Using a DependencyProperty as the backing store for Route.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RouteProperty =
            DependencyProperty.Register( nameof( Route ), typeof( RouteLookupSummary ), typeof( RouteLookupComboBox ), new PropertyMetadata( null ) );

        public event Action<object, SelectionChangedEventArgs> SelectionChanged;

        public event PropertyChangedEventHandler PropertyChanged;

        [ NotifyPropertyChangedInvocator ]
        protected virtual void OnPropertyChanged( [ CallerMemberName ] string propertyName = null )
        {
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( propertyName ) );
        }

        private RouteLookupVirtualObservableCollection RouteCollection;

        private void UserControl_Initialized( object sender, EventArgs e )
        {
            LookupCombo.ItemsSource = RouteCollection = new RouteLookupVirtualObservableCollection();
        }

        protected virtual void OnSelectionChanged( object arg1, SelectionChangedEventArgs arg2 )
        {
            SelectionChanged?.Invoke( arg1, arg2 );
        }

        private void LookupCombo_SelectionChanged(object arg1, SelectionChangedEventArgs arg2)
        {
            Route = RouteCollection.SelectedItem?.Item;
            OnSelectionChanged(this, arg2);
        }
    }
}